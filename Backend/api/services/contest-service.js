import serviceErrors from './service-errors.js';
import bcrypt from 'bcrypt';
import { roles, pool } from '../config.js';
import contestData from '../data/contest-data.js';


const createContest = contestData => {
    return async (info) => {

        if (await contestData.titleExists(info)) {
            return { error: serviceErrors.RECORD_DUPLICATE }
        }
        const contest = await contestData.createContest({
            users_id: info.users_id,
            title: info.title,
            type: info.type,
            phase1_limit: info.phase1_limit,
            phase2_limit: info.phase2_limit,
            jury: info.jury,
            category: info.category
        })
        if (info.type === "Invitational") {
            await contestData.invitedUsers(info);
        }
        const getTime = async () => {
            const startDate = await contest.start_date;
            const endDate = await contest.end_date;
            const seconds = (endDate.getTime() - startDate.getTime());
            return seconds
        }
        const timer = await getTime();

        setTimeout(() => getTopThreeAndPoints(contestData)(contest), timer);
        if (contest === undefined) {
            return { error: serviceErrors.RECORD_NOT_FOUND, contest: null }
        } else {
            return { error: null, contest: contest }
        }
    }
}

const getContestPhase1 = () => {
    return async () => {
        const contestPhase1 = await contestData.contestsPhase1();
        if (contestPhase1 === undefined) {
            return { error: serviceErrors.RECORD_NOT_FOUND, contests: null }
        } else {
            return { error: null, contests: contestPhase1 };
        }
    }
}

const getContestPhase2 = () => {
    return async () => {
        const contestPhase2 = await contestData.contestsPhase2();
        if (contestPhase2 === undefined) {
            return { error: serviceErrors.RECORD_NOT_FOUND, contests: null }
        } else {
            return { error: null, contests: contestPhase2 };
        }
    }
}

const getContestFinished = () => {
    return async () => {
        const contestsFinished = await contestData.contestsFinished();
        if (contestsFinished === undefined) {
            return { error: serviceErrors.RECORD_NOT_FOUND, contests: null }
        } else {
            return { error: null, contests: contestsFinished };
        }
    }
}

const getContestPhase = contestData => {
    return async contestId => {
        const phase = await contestData.getContestPhase(contestId);
        if (phase === undefined) {
            return { error: serviceErrors.RECORD_NOT_FOUND, phase: phase }
        } else {
            return { error: null, phase: phase };
        }
    }
}

const getContests = contestData => {
    return async () => {
        const contests = await contestData.getContests();
        return { error: null, contests: contests };
    }
}

const getContestById = contestData => {
    return async (contestId) => {
        const contests = await contestData.getContestById(contestId);
        return { error: null, contest: contests };
    }
}

const activeContestsByJunkie = contestData => {
    return async (junkieId) => {
        const contests = await contestData.activeContestsByJunkie(junkieId);
        return { error: null, contests: contests }
    }
}

const finishedContestsByJunkie = contestData => {
    return async (junckieId) => {
        const contests = await contestData.finishedContestsByJunkie(junckieId);
        return { error: null, contests: contests }
    }
}

const submission = contestData => {
    return async (submitObj) => {
        const submission = await contestData.submission(submitObj);
        return { error: null, submission: submission };
    }
}

const getJunkieScores = contestData => {
    return async (id) => {
        const scores = await contestData.getJunkieScores(id);
        return { error: null, scores: scores };
    }
}

const getAllOtherJunkieScores = contestData => {
    return async (id) => {
        const scores = await contestData.getAllOtherJunkieScores(id);
        return { error: null, scores: scores };
    }
}

const certainSubmissionScore = contestData => {
    return async (submissionId) => {
        const score = await contestData.certainSubmissionScore(submissionId);
        return { error: null, score: score };
    }
}

const getTopThreeAndPoints = contestData => {
    return async (contest) => {
        const first = await contestData.getFirstPlace(contest);
        const second = await contestData.getSecondPlace(contest);
        const third = await contestData.getThirdPlace(contest);
        if (third.length > 1) {
            third.map(async element => {
                return (await contestData.sharedThirdPoints(+element.users_id))
            });
        } else if (third.length === 1) { await contestData.thirdPoints(third[0].users_id); }



        if (first.length > 1) {
            first.map(async element => {
                return (await contestData.sharedFirstPoints(+element.users_id))
            });
        } else if (first.length === 1) {
            if (first[0].average_score > 2 * second[0].average_score) {
                await contestData.firstBigPoints(+first[0].users_id);
            } else { await contestData.firstPoints(first[0].users_id); }
        }


        if (second.length > 1) {
            second.map(async element => {
                return (await contestData.sharedSecondPoints(+element.users_id))
            });
        } else if (second.length === 1) {
            await contestData.secondPoints(second[0].users_id);
        }
    }
}

const getSubmissionsByJunkieId = contestData => {
    return async (junkieId) => {
        const submissions = await contestData.getSubmissionsByJunkieId(junkieId);
        return { error: null, submissions: submissions };
    }
}

const getSubmissionByContestIdAndJunkieId = contestData => {
    return async (junkieId, contestId) => {
        const submission = await contestData.getSubmissionByContestIdAndJunkieId(junkieId, contestId);
        return { error: null, submission: submission };
    }
}

const getSingleContestP1 = contestData => {
    return async (contestId) => {
        const contest = await contestData.getSingleContestP1(contestId);
        return { error: null, contest: contest };
    }
}

export default {
    createContest,
    getContests,
    getContestById,
    activeContestsByJunkie,
    finishedContestsByJunkie,
    getContestPhase1,
    getContestPhase2,
    getContestFinished,
    submission,
    getJunkieScores,
    getAllOtherJunkieScores,
    certainSubmissionScore,
    getTopThreeAndPoints,
    getSubmissionsByJunkieId,
    getSubmissionByContestIdAndJunkieId,
    getSingleContestP1,
    getContestPhase
}