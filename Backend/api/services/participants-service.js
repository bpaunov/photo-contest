import serviceErrors from './service-errors.js';
import contestData from '../data/contest-data.js';

const participate = participantsData => {
    return async (junkieId, contestId) => {
        const contestExists = await contestData.getContestById(contestId);
        if (!contestExists) {
            return { error: serviceErrors.RECORD_NOT_FOUND, contest: null };
        }
        const participationExists =
            await participantsData.participationExists(junkieId, contestId);
        if (participationExists) {
            return { error: serviceErrors.RECORD_ALREADY_EXISTS, participation: null };
        }
        const participation = await participantsData.participate(junkieId, contestId);
        return { error: null, data: participation };
    }
}

export default {
    participate
}