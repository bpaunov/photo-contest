import serviceErrors from './service-errors.js';
import juryData from '../data/jury-data.js';
import contestData from '../data/contest-data.js'

const ratePhoto = juryData => {
    return async (info) => {

        const ratingExists = await juryData.ratingExists(info)
        if (ratingExists) {
            return {error:serviceErrors.RECORD_ALREADY_EXISTS, rating:null}
        }
        const rate = await juryData.ratePhoto({
            score: info.score,
            comment: info.comment,
            not_fit: info.not_fit,
            submissions_id: info.submissions_id,
            users_id: info.users_id
        })
        if (!rate) {
            return { error: serviceErrors.RECORD_NOT_FOUND, rating: null }
        } else {
            return { error: null, rating: rate }
        }
    }
}

const viewAllSubmissionsFinished = juryData => {
    return async (contestId) => {
        const submissions = await juryData.viewAllSubmissionsFinished(contestId)

        if (!submissions) {
            return { error: serviceErrors.RECORD_NOT_FOUND, submissions: null }
        } else {
            return { error: null, submissions: submissions }
        }
    }
}
const viewSubmissionsPhase1ByContest = juryData => {
    return async (contestId) => {
        const submissions = await juryData.viewSubmissionsPhase1ByContest(contestId)
        if (!submissions) {
            return { error: serviceErrors.RECORD_NOT_FOUND, submissions: null }
        } else {
            return { error: null, submissions: submissions }
        }
    }
}

const viewSubmissionsPhase2ByContest = juryData => {
    return async (contestId) => {
        const submissions = await juryData.viewSubmissionsPhase2ByContest(contestId)
        if (!submissions) {
            return { error: serviceErrors.RECORD_NOT_FOUND, submissions: null }
        } else {
            return { error: null, submissions: submissions }
        }
    }
}

const viewSubmissions = juryData => {

    return async (contestId) => {
        const submissions = await juryData.viewSubmissions(contestId)
        if (!submissions) {
            return { error: serviceErrors.RECORD_NOT_FOUND, submissions: null }
        } else {
            return { error: null, submissions: submissions }
        }
    }
}

export default {
    ratePhoto,
    viewAllSubmissionsFinished,
    viewSubmissionsPhase1ByContest,
    viewSubmissionsPhase2ByContest,
    viewSubmissions
}