import serviceErrors from './service-errors.js';
import bcrypt from 'bcrypt';
import { roles, pool } from '../config.js';

const getUserByUsername = userData => {
    return async (username) => {
        const user = await userData.getUserByUsername(username);
        if (!user) {
            return { error: serviceError.RECORD_NOT_FOUND, user: null }
        }
        return { error: null, user: user };
    }
}

const CreateUser = userData => {

    return async (userInfo) => {
        const cryptPass = await bcrypt.hash(userInfo.password, 10);
        const check = await userData.getUserByUsername(userInfo.username);

        if (check !== undefined) {
            return { error: serviceErrors.RECORD_DUPLICATE, user: null };
        } else {
            const user = await userData.createUser({
                username: userInfo.username,
                password: cryptPass,
                first_name: userInfo.first_name,
                last_name: userInfo.last_name,
                role: roles.COMMON.JUNKIE,
            })
            return { error: null, user: user };
        }
    }
}

const getUsersByRank = userData => {
    return async () => {
        const users = await userData.getJunkiesByRank()
        if (users === undefined) {
            return { error: serviceErrors.RECORD_NOT_FOUND, users: null }
        } else {
            return { error: null, users: users }
        }
    }
}

const getCurrentPoints = userData => {
    return async(id) => {
        const points = await userData.getCurrentPoints(id)
        if (points === undefined) {
            return { error: serviceErrors.RECORD_NOT_FOUND, points: null }
        }
        let rankingInfo = {
            rank:"",
            points: points.points,
            pointsUntilNext: 0
        }
        if (points.points <= 50){
            rankingInfo.rank = 'Junkie'
            rankingInfo.pointsUntilNext = 51 - points.points
        } else if (points.points <= 150){
            rankingInfo.rank = 'Enthusiast'
            rankingInfo.pointsUntilNext = 151 - points.points
        } else if (points.points <= 1000){
            rankingInfo.rank = 'Master'
            rankingInfo.pointsUntilNext = 1001 - points.points
        } else {
            rankingInfo.rank = "Wise and Benevolent Photo Dictator"
            rankingInfo.pointsUntilNext = "Max rank reached."
        }
        return { error: null, rankingInfo: rankingInfo }
    }
}

export default {
    getUserByUsername,
    CreateUser,
    getUsersByRank,
    getCurrentPoints
}