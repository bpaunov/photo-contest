import bcrypt from 'bcrypt';
import serviceError from './service-errors.js';

const login = userData => {
    return async (username, password) => {
        const checkIfUserExists = await userData.getUserByUsername(username);
        
        if (!checkIfUserExists) {
            return { error: serviceError.RECORD_NOT_FOUND, user: null }
        }
        const checkIfPasswordIsValid = await bcrypt.compare(password, checkIfUserExists.password);
        if (!checkIfPasswordIsValid) {
            return { error: serviceError.RECORD_NOT_FOUND, user: null }
        }
        return { error: null, session: true };
    }
}

export default {
    login
}