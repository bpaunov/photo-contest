import express from 'express';
import userData from '../data/user-data.js';
import sessionService from '../services/session-service.js';
import userService from '../services/user-service.js'
import serviceError from '../services/service-errors.js';
import createToken from '../auth/createToken.js';
import { redisClient } from '../config.js';
import { authMiddleware } from '../auth/auth-middleware.js';
import { authentication } from '../middlewares/token-check.js';
import atob from 'atob';

const sessionController = express.Router();

sessionController

    .post('/', async (req, res) => {
        const { username, password } = req.body;
        const createdSession = await sessionService.login(userData)(username, password);

        if (createdSession.error === serviceError.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Your username/password is incorrect' })
        } else if (createdSession.error === serviceError.RECORD_ALREADY_EXISTS) {
            res.status(404).send({ message: 'Something went wrong' });
        } else {
            const user = await userService.getUserByUsername(userData)(username);

            const payload = {
                sub: user.user.id,
                username: username,
                role: user.user.is_organizer
            }
            const createdToken = createToken(payload);
            res.status(200).send({ token: createdToken });
        }
    })

    .delete('/', authMiddleware, authentication, async (req, res) => {
        const token = req.headers.authorization.split(' ')[1];
        const userId = JSON.parse(atob(token.split('.')[1])).sub;

        redisClient.HSET('blacklistedTokens', token, userId);
        res.status(200).send({ message: 'Logout successful' });
    })

export default sessionController;