import { authMiddleware } from "../auth/auth-middleware.js";
import juryData from "../data/jury-data.js";
import juryService from "../services/jury-service.js";
import serviceErrors from "../services/service-errors.js";
import express from 'express';
import atob from 'atob';
import { authentication } from "../middlewares/token-check.js";
import contestService from "../services/contest-service.js";
import contestData from "../data/contest-data.js";

const juryController = express.Router();

juryController.
    post('/score', authMiddleware, authentication, async (req, res) => {
        const { ...body } = req.body;
        const token = req.headers.authorization.split(' ')[1];
        const users_id = (JSON.parse(atob(token.split('.')[1]))).sub;
        const score = await juryService.ratePhoto(juryData)({
            users_id: +users_id,
            score: body.score,
            not_fit: body.not_fit,
            submissions_id: body.submissions_id,
            comment: body.comment,
        })
        if (score.error === serviceErrors.RECORD_ALREADY_EXISTS) {
            return res.status(400).send({ message: 'Submission already rated' })
        } if (score.error === serviceErrors.RECORD_NOT_FOUND) {
            return res.status(400).send({ message: 'There was a problem with scoring. Check information and try again.' })
        }
        res.status(200).send(score.rating);
    }).get('/contests/:contestId/submissions-phase1', authMiddleware, authentication, async (req, res) => {
        const {contestId} = req.params;
        const submissions = await juryService.viewSubmissionsPhase1ByContest(juryData)(+contestId);
        if (submissions.error === serviceErrors.RECORD_NOT_FOUND) {
            return res.status(400).send({ message: 'No submissions in Phase 1 for this contest' })
        } else {
            return res.status(200).send(submissions.submissions);
        }
    }).get('/contests/:contestId/submissions-finished', authMiddleware, authentication, async (req, res) => {
        const {contestId} = req.params;
        const submissions = await juryService.viewAllSubmissionsFinished(juryData)(contestId);
        if (submissions.error === serviceErrors.RECORD_NOT_FOUND) {
            return res.status(400).send({ message: 'No submissions currently in Finished phase' })
        } else {
            return res.status(200).send(submissions.submissions)
        }
    }).get('/contests/:contestId/submissions-phase2', authMiddleware, authentication, async (req, res) => {
        const {contestId} = req.params;
        const submissions = await juryService.viewSubmissionsPhase2ByContest(juryData)(+contestId);
        if (submission.error === serviceErrors.RECORD_NOT_FOUND) {
            return res.status(400).send({ message: 'No submissions in Phase 2 for this contest' })
        } else {
            return res.status(200).send(submissions.submissions);
        }
    })
    .get('/contests/:contestId/submissions', authMiddleware, authentication, async (req, res) => {
        const {contestId} = req.params;
        const submissions = await juryService.viewSubmissions(juryData)(+contestId);

        if (submissions.error === serviceErrors.RECORD_NOT_FOUND) {
            return res.status(400).send({ message: 'No submissions' })
        } else {
            return res.status(200).send(submissions.submissions);
        }
    })
    .get('/contests/:contestId/phase', authMiddleware, authentication, async (req, res) => {
        const {contestId} = req.params;
        const phase = await contestService.getContestPhase(contestData)(+contestId);
        if (phase.error === phase.RECORD_NOT_FOUND) {
            return res.status(400).send({ message: 'Something went terribly wrong.' })
        } else {
            return res.status(200).send(phase.phase);
        }
    })


export default juryController;

