import express from 'express';
import contestData from '../data/contest-data.js';
import userData from '../data/user-data.js';
import contestService from '../services/contest-service.js';
import serviceError from '../services/service-errors.js';
import userService from '../services/user-service.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import atob from 'atob';
import { authentication } from '../middlewares/token-check.js';


const organizerController = express.Router();

organizerController.
    post('/contests', authMiddleware, authentication, async (req, res) => {

        const { ...body } = req.body;
        const token = req.headers.authorization.split(' ')[1];
        const users_id = (JSON.parse(atob(token.split('.')[1]))).sub;
        const contest = await contestService.createContest(contestData)({
            users_id: +users_id,
            title: body.title,
            type: body.type,
            invited: body.invited,
            phase1_limit: body.phase1_limit,
            phase2_limit: body.phase2_limit,
            jury: body.jury,
            category: body.category
        });
        if (contest.error === serviceError.RECORD_DUPLICATE) {
            return res.status(402).send({ message: 'There is already a contest with that title!' })
        }
        res.status(200).send(contest.contest);
    })
    .get('/junkies', authMiddleware, authentication, async (req, res) => {
        const users = await userService.getUsersByRank(userData)();

        if (users.error === serviceError.RECORD_NOT_FOUND) {
            return res.status(400).send({ message: 'Something went wrong.' })
        } else {
            res.status(200).send(users.users)
        }
    }).get('/contests/phase1', authMiddleware, authentication, async (req, res) => {
        const contestsPhase1 = await contestService.getContestPhase1(contestData)();
        if (contestsPhase1.error === serviceError.RECORD_NOT_FOUND) {
            return res.status(400).send({ message: 'No contests found in Phase 1.' })
        } else {
            res.status(200).send(contestsPhase1.contests)
        }
    }).get('/contests/phase2', authMiddleware, authentication, async (req, res) => {
        const contestsPhase2 = await contestService.getContestPhase2(contestData)();
        if (contestsPhase2.error === serviceError.RECORD_NOT_FOUND) {
            return res.status(400).send({ message: 'No contests found in Phase 2.' })
        } else {
            res.status(200).send(contestsPhase2.contests)
        }

    }).get('/contests/finished', authMiddleware, authentication, async (req, res) => {
        const contestsFinished = await contestService.getContestFinished(contestData)();
        if (contestsFinished.error === serviceError.RECORD_NOT_FOUND) {
            return res.status(400).send({ message: 'No contests finished contests found' })
        } else {
            res.status(200).send(contestsFinished.contests)
        }
    })
    .get('/submissions', authMiddleware, authentication, async (req, res) => {
        const { junkieId } = req.query;
        const submissions = await (await contestService.getSubmissionsByJunkieId(contestData)(junkieId)).submissions;
        res.status(200).send(submissions);
    })
    .get('/contests/:contestId/phase1', authMiddleware, authentication, async (req, res) => {
        const { contestId } = req.params;
        const contest = await (await contestService.getSingleContestP1(contestData)(contestId)).contest;
        res.status(200).send(contest)
    })

export default organizerController;