import express from 'express';
import serviceError from '../services/service-errors.js';
import contestService from '../services/contest-service.js';
import contestData from '../data/contest-data.js';
import participantsService from '../services/participants-service.js';
import participantsData from '../data/participants-data.js';
import { authMiddleware } from '../auth/auth-middleware.js';
import atob from 'atob';
import { authentication } from '../middlewares/token-check.js';
import multer from 'multer';
import fs from 'fs';
import userData from '../data/user-data.js';
import userService from '../services/user-service.js';
import { get } from 'http';

const upload = multer({
    dest: "../../photos_of_photo_contest",
    //validation here (not finished yet)
    // fileFilter: (req, file, cb) => {
    //     const fileExtension = file.originalname.split('.')[(file.originalname.split('.').length) - 1];
    //     if (fileExtension === 'png' || fileExtension === 'jpg' || fileExtension === 'jpeg') {
    //         cb(null, true);
    //     } else {
    //         cb(null, false);
    //         return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
    //     }
    // }
});

const junkieController = express.Router();

junkieController
    .post('/', async (req, res) => {
        const { ...body } = req.body;
        const user = await userService.CreateUser(userData)(body);

        if (user.error === serviceError.RECORD_DUPLICATE) {
            return res.status(409).send({ message: 'Username already taken.' });
        } else {
            res.status(200).send(user.user)
        }
    })
    .get('/contests', authMiddleware, authentication, async (req, res) => {
        const contests = await (await contestService.getContests(contestData)()).contests;
        const filteredContests = contests;

        res.status(200).send(filteredContests);
    })
    .post('/contests/:contestId/participate', authMiddleware, authentication, async (req, res) => {
        const { contestId } = req.params;
        const token = req.headers.authorization.split(' ')[1];
        const junkieId = JSON.parse(atob(token.split('.')[1])).sub;
        const participation = await participantsService.participate(participantsData)(junkieId, contestId);

        participation.error === serviceError.RECORD_NOT_FOUND ?
            res.status(404).send({ message: 'Not found' }) :
            participation.error === serviceError.RECORD_ALREADY_EXISTS ?
                res.status(404).send({ message: 'Already participates.' }) :
                res.status(200).send(participation.data);
    })

    .get('/contests/participations', authMiddleware, authentication, async (req, res) => {
        const token = req.headers.authorization.split(' ')[1];
        const junkieId = JSON.parse(atob(token.split('.')[1])).sub;
        const contests = await (await contestService.activeContestsByJunkie(contestData)(junkieId)).contests;;
        res.status(200).send(contests);
    })

    .get('/contests-finished', authMiddleware, authentication, async (req, res) => {
        const token = req.headers.authorization.split(' ')[1];
        const junckieId = JSON.parse(atob(token.split('.')[1])).sub;
        const contests = await (await contestService.finishedContestsByJunkie(contestData)(junckieId)).contests;

        res.status(200).send(contests);
    })

    .post('/contests/:contestId/submit', authMiddleware, upload.single('photo'), async (req, res) => {
        const { contestId } = req.params;
        const { title, story } = req.body;
        const token = req.headers.authorization.split(' ')[1];
        const junkieId = JSON.parse(atob(token.split('.')[1])).sub;
        const submission = await contestService.submission(contestData)({
            userId: junkieId,
            contestId: contestId,
            title: title,
            story: story,
            img: `${req.file.filename}.jpg`
        });
        fs.rename(`../../photos_of_photo_contest/${req.file.filename}`, `../../photos_of_photo_contest/${req.file.filename}.jpg`, (err) => {
            if (err) throw err;
        });
        res.status(200).send(submission.submission);
    })
    .get('/submissions/:submissionId/scores', authMiddleware, authentication, async (req, res) => {
        const { submissionId } = req.params;
        const scores = await (await contestService.certainSubmissionScore(contestData)(submissionId)).score;
        res.status(200).send(scores);
    })
    .get('/submissions/scores', authMiddleware, authentication, async (req, res) => {
        const token = req.headers.authorization.split(' ')[1];
        const junkieId = JSON.parse(atob(token.split('.')[1])).sub;

        const scores = await (await contestService.getJunkieScores(contestData)(junkieId)).scores;
        res.status(200).send(scores);
    })
    .get('/contest/:contestId/scores', authMiddleware, authentication, async (req, res) => {
        const token = req.headers.authorization.split(' ')[1];
        const junkieId = JSON.parse(atob(token.split('.')[1])).sub;

        const scores = await (await contestService.getAllOtherJunkieScores(contestData)(junkieId)).scores;
        res.status(200).send(scores);
    })
    .get('/ranking', authMiddleware, authentication, async (req, res) => {
        const token = req.headers.authorization.split(' ')[1];
        const junkieId = JSON.parse(atob(token.split('.')[1])).sub;

        const rankingInfo = await (await userService.getCurrentPoints(userData)(junkieId)).rankingInfo;
        res.status(200).send(rankingInfo);
    })
    .get('/contests/phase1', authMiddleware, authentication, async (req, res) => {
        res.redirect(302, `http://localhost:3000/organizers/contests/phase1`);
    })
    .get('/submissions', authMiddleware, authentication, async (req, res) => {
        const { junkieId } = req.query;
        res.redirect(302, `http://localhost:3000/organizers/submissions/?junkieId=${junkieId}`);
    })
    .get('/contests/:contestId/submission', authMiddleware, authentication, async (req, res) => {
        const { contestId } = req.params;
        const token = req.headers.authorization.split(' ')[1];
        const junkieId = JSON.parse(atob(token.split('.')[1])).sub;

        const submission = await (await contestService.getSubmissionByContestIdAndJunkieId(contestData)(junkieId, contestId)).submission;
        res.status(200).send(submission);
    })
    .get('/contests/:contestId/phase1', authMiddleware, authentication, async (req, res) => {
        const { contestId } = req.params;
        res.redirect(302, `http://localhost:3000/organizers/contests/${contestId}/phase1`)
    })
    .get('/contests/:contestId', authMiddleware, authentication, async (req, res) => {
        const { contestId } = req.params;
        const contest = await (await contestService.getContestById(contestData)(contestId)).contest;
        res.status(200).send(contest);
    })
export default junkieController;