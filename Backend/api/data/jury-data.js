import { pool } from '../config.js';

const viewSubmissionsPhase1ByContest = async (contestId) => {
    const submissions = await pool.query(`
    SELECT * 
    FROM  submissions	
    WHERE contests_id in (
    SELECT contests_id
    FROM phase_1
    WHERE end_date > NOW()
    )
    && contests_id = ${contestId};
    `)
    return [...submissions];
}

const viewSubmissionsPhase2ByContest = async (contestId) => {
    const submissions = await pool.query(`
    SELECT * 
    FROM  submissions	
    WHERE contests_id in (
    SELECT contests_id
    FROM phase_2
    WHERE end_date > NOW()
    )
    && contests_id = ${contestId};
    `)
    return [...submissions];
}

const viewAllSubmissionsFinished = async (contestId) => {
    const submission = await pool.query(`
    SELECT * 
    FROM  submissions	
    WHERE  contests_id=${contestId})
    ORDER BY average_score DESC
    `)
    return submission;
}

const ratePhoto = async (info) => {
    if (info.not_fit == '1') {
        await pool.query(`
    UPDATE scores
    SET
    score = 0,
    comment = 'Wrong category for this submission',
    not_fit = true,
    is_touched = 1
    WHERE submissions_id = ${info.submissions_id}
    && users_id = ${info.users_id};`)
        await pool.query(`
    UPDATE submissions
    SET average_score = (SELECT avg(score) 
                        FROM scores
                        WHERE submissions_id LIKE ${info.submissions_id})
    WHERE id like ${info.submissions_id}`)

        const rate = await pool.query(`SELECT * 
    FROM scores 
    WHERE users_id = ${info.users_id}
    && submissions_id =${info.submissions_id}
        `)
        return rate[0];
    }
    await pool.query(`
    UPDATE scores
    SET
    score = ${info.score},
    comment = '${info.comment}',
    not_fit = ${info.not_fit ? info.not_fit : false},
    is_touched = 1
    WHERE submissions_id = ${info.submissions_id}
    && users_id = ${info.users_id};`)

    await pool.query(`
    UPDATE submissions
    SET average_score = (SELECT avg(score) 
                        FROM scores
                        WHERE submissions_id LIKE ${info.submissions_id})
    WHERE id like ${info.submissions_id}`)

    const rate = await pool.query(`SELECT *
    FROM scores 
    WHERE users_id = ${info.users_id}
    && submissions_id = ${info.submissions_id};
    `)
    return rate[0];
}

const ratingExists = async (info) => {
    const exists = await pool.query(`
    SELECT *
    FROM  scores
    WHERE users_id = ${info.users_id}
    && submissions_id =${info.submissions_id}
    && is_touched = 1
    `)
    return exists[0];
}

const viewSubmissions = async (contestId) => {
    const submission = await pool.query(`
    SELECT *
    FROM submissions 
    WHERE contests_id = ${contestId};
    `)
    return [...submission];
}
export default {
    ratePhoto,
    ratingExists,
    viewAllSubmissionsFinished,
    viewSubmissionsPhase1ByContest,
    viewSubmissionsPhase2ByContest,
    viewSubmissions
}