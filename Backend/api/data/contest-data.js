import { pool } from '../config.js';
import { types } from '../config.js';

const updateType = async () => {
    const update = await pool.query(`
    UPDATE contests
    SET type = 'Invitational'
    WHERE id=(SELECT MAX(id) FROM contests);
    `)
}
const titleExists = async (info) => {
    const title = await pool.query(`
    SELECT title
    FROM contests
    WHERE title = '${info.title}'
    `)
    return title[0]
}
const createPhase1 = async (info) => {
    const phase1 = await pool.query(`
    INSERT INTO phase_1
    (contests_id, start_date, end_date)
    VALUES
    ((SELECT MAX(id) FROM contests), NOW(), (SELECT DATE_ADD(NOW(), INTERVAL ${info.phase1_limit} MINUTE)))
    `)
};

const createPhase2 = async (info) => {
    const phase2 = await pool.query(`
    INSERT INTO phase_2
    (contests_id, start_date, end_date)
    VALUES
    ((SELECT MAX(id) FROM contests), NOW(), (SELECT DATE_ADD(NOW(), INTERVAL ${+info.phase2_limit + +info.phase1_limit} MINUTE)))
    `)
};

const invitedUsers = async (info) => {
    info.invited.forEach(async element => await pool.query(`
    INSERT INTO participations
    (users_id, contests_id, date_joined, is_deleted)
    VALUES
    (${element}, (SELECT MAX(id) from contests), NOW(), 0);
    UPDATE users
    SET points = points + 3
    WHERE id = ${element};
    `))

}


const createJury = async (info) => {
    const mastersIds = await pool.query(`
    SELECT id
    FROM users
    WHERE points>=151 
    `)
    const idsArray = [];
    [...mastersIds].map(el => { idsArray.push(el.id) });
    if (info.jury) {
        info.jury.forEach(async (el) => {
            if (idsArray.includes(el)) {
                await pool.query(`
                INSERT INTO jurors
                (users_id, contests_id, date_joined)
                SELECT ${el} as users_id,(SELECT MAX(id) FROM contests), NOW() 
                `)
            }
        });
    }

    const jury = await pool.query(`
        INSERT INTO jurors
        (users_id, contests_id, date_joined)
        SELECT users.id as users_id,(SELECT MAX(id) FROM contests), NOW()
        FROM users
        WHERE  is_organizer = 1;
        SELECT * FROM jurors WHERE contests_id=(SELECT MAX(id) FROM contests);
    `);
    return jury;
};

const createContest = async (info) => {

    const create = await pool.query(`
        INSERT INTO contests
        (  start_date, end_date, users_id, title, category)
        VALUES (  NOW(), (SELECT NOW() + INTERVAL ${info.phase1_limit} MINUTE + INTERVAL ${info.phase2_limit} MINUTE),  ${info.users_id}, '${info.title}', '${info.category}');
        
        `)
    const phase1 = await createPhase1(info);
    const phase2 = await createPhase2(info);
    const jury = await createJury(info);
    if (info.type) {
        const type = await updateType();
    }
    const contest = await pool.query(`
    SELECT * 
    from contests 
    WHERE id = (SELECT MAX(id) 
                FROM contests);
    `)
    return contest[0];
};

const activeContestsByOrganizer = async (username) => {
    const contests = await pool.query(`
    SELECT *
    FROM contests
    WHERE users_id = (SELECT id FROM users WHERE username = '${username}')
    `);
    return contests;
};

const allContestsByOrganizer = async (username) => {
    const contests = await pool.query(`
    SELECT *
    FROM contests
    WHERE users_id = (SELECT id FROM users WHERE username = '${username}')
    `);
    return contests;
};

const inactiveContestsByOrganizer = async (username) => {
    const contests = await pool.query(`
    SELECT *
    FROM contests
    WHERE users_id = (SELECT id FROM users WHERE username = '${username}')
    `);
    return contests;
};

const getContestById = async (id) => {
    const contest = await pool.query(`
        select * from contests
        where id = ${id}
    `);
    return contest[0];
};

const getContests = async () => {
    const contests = await pool.query(`
        select con.id, con.start_date, con.end_date, con.title, con.category from contests con
        join phase_1 p1 on p1.contests_id = con.id
        where now() < p1.end_date
        && type = 'Open'
        ORDER BY end_date asc;
        `)
    return [...contests];
};

const activeContestsByJunkie = async (junkieId) => {
    const contests = await pool.query(`
        select con.id, con.start_date, con.end_date, con.category,
            con.title, p.id as participantId, u.id as userId from contests as con
        join participations p on p.contests_id = con.id
        join users u on u.id = p.users_id
        where u.id = ${junkieId}
        order by con.title;
    `);
    return [...contests];
};

const finishedContestsByJunkie = async (junkieId) => {
    const contests = await pool.query(`
        select con.id as contestId, con.start_date, con.end_date,
            con.title, p.id as participantId, u.id as userId from contests as con
        join participations p on p.contests_id = con.id
        join users u on u.id = p.users_id
        where con.end_date < NOW() && u.id = ${junkieId}
        order by con.title;
    `);
    return [...contests];
};

const contestsPhase1 = async () => {
    const contests = await pool.query(`
    SELECT c.*
    FROM contests c
    join phase_1 p
    where p.contests_id = c.id
    && p.end_date > NOW();
    `)
    return contests;
};

const contestsPhase2 = async () => {
    const contests = await pool.query(`
    SELECT c.*
    FROM contests c
    join phase_2 p2, phase_1 p1
    where p1.contests_id =c.id
    && p2.contests_id = c.id
    && p2.end_date > NOW()
    && p1.end_date < NOW();
    `)
    return contests;
};

const contestsFinished = async () => {
    const contests = await pool.query(`
    SELECT c.*
    FROM contests c
    join phase_2 p2, phase_1 p1
    where p1.contests_id =c.id
    && p2.contests_id = c.id
    && p2.end_date < NOW()
    && p1.end_date < NOW();
    `)
    return contests;
};

const certainContestJury = async (contestId) => {
    const jury = await pool.query(`
        select users_id from jurors
        where contests_id = ${contestId};
    `);
    return [...jury];
}

const submission = async (submitObj) => {
    await pool.query(`
        insert into submissions
        (users_id, title, story, img, contests_id, submitted_on)
        values (${submitObj.userId}, '${submitObj.title}', '${submitObj.story}', '${submitObj.img}', ${submitObj.contestId}, now())
    `);
    const createdSubmission = await pool.query(`
        select * from submissions WHERE id=(SELECT MAX(id) FROM submissions);
    `);
    const jury = await certainContestJury(submitObj.contestId);
    jury.forEach(async (judge) => {
        await pool.query(`
            insert into scores
            (score, submissions_id, users_id, not_fit)
            values (3, ${createdSubmission[0].id}, ${judge.users_id}, 0)
        `);
    })
    return createdSubmission[0];
}

const getJunkieScores = async (id) => {
    const allScores = await pool.query(`
        select s.score, s.comment, sub.img, sub.submitted_on, sub.users_id, u.username from scores s
        join submissions sub on s.submissions_id = sub.id
        join contests c on c.id = sub.contests_id
        join users u on u.id = sub.users_id
        where c.end_date < now() && u.id = ${id}
        order by sub.id desc;
    `);
    return [...allScores];
}

const getAllOtherJunkieScores = async (id) => {
    const allScores = await pool.query(`
        select s.score, s.comment, sub.img, sub.submitted_on, sub.users_id, u.username from scores s
        join submissions sub on s.submissions_id = sub.id
        join contests c on c.id = sub.contests_id
        join users u on u.id = sub.users_id
        where c.end_date < now() && u.id != ${id}
        order by sub.id desc;
    `);
    return [...allScores];
}

const certainSubmissionScore = async (submissionId) => {
    const score = await pool.query(`
    select s.score, s.comment, u.username as juror from scores s
	join submissions sub on sub.id = s.submissions_id
    join users u on u.id = s.users_id
    where sub.id = ${submissionId};
    `);
    return [...score];
}

const getFirstPlace = async (contest) => {
    const first = await pool.query(`
    SELECT *
    FROM submissions
    WHERE contests_id = ${contest.id}
    && average_score = (SELECT MAX(average_score) 
                        FROM submissions 
                        WHERE contests_id = ${contest.id})
    ORDER BY id;
    `)
    return [...first];
}

const getSecondPlace = async (contest) => {
    const second = await pool.query(`
    SELECT *
    FROM submissions
    WHERE average_score = (SELECT MAX(average_score)
                            FROM submissions 
                            WHERE average_score < (SELECT MAX(average_score) 
                                                    FROM submissions 
                                                    WHERE contests_id = ${contest.id})
                            && contests_id = ${contest.id})
    && contests_id = ${contest.id}
    ORDER BY id;
    `)
    return [...second];
}

const getThirdPlace = async (contest) => {
    const third = await pool.query(`
    SELECT *
    FROM submissions 
    WHERE average_score = (SELECT MAX(average_score) 
				FROM submissions 
                WHERE average_score < (SELECT MAX(average_score) 
								FROM submissions 
                                WHERE average_score < (SELECT MAX(average_score) 
                                                FROM submissions 
                                                WHERE contests_id = ${contest.id})
                                && contests_id = ${contest.id})
                && contests_id = ${contest.id})
    && contests_id = ${contest.id}
    ORDER BY id
    `)
    return [...third];
}

const sharedThirdPoints = async (id) => {
    const award = await pool.query(`
    UPDATE users
    SET points = points + 10
    WHERE id = ${id}
    `)
}
const thirdPoints = async (id) => {
    const award = await pool.query(`
    UPDATE users
    SET points = points + 20
    WHERE id = ${id}
    `)
}
const sharedSecondPoints = async (id) => {

    const award = await pool.query(`
    UPDATE users
    SET points = points + 25
    WHERE id = ${id}
    `)
}
const secondPoints = async (id) => {
    const award = await pool.query(`
    UPDATE users
    SET points = points + 35
    WHERE id = ${id}
    `)
}
const sharedFirstPoints = async (id) => {
    const award = await pool.query(`
    UPDATE users
    SET points = points + 40
    WHERE id = ${id}
    `)
}
const firstBigPoints = async (id) => {
    const award = await pool.query(`
    UPDATE users
    SET points = points + 75
    WHERE id = ${id}
    `)
}
const firstPoints = async (id) => {
    const award = await pool.query(`
    UPDATE users
    SET points = points + 50
    WHERE id = ${id}
    `)
}

const getSubmissionsByJunkieId = async (junkieId) => {
    const submissions = await pool.query(`
        select * from submissions
        where users_id = ${junkieId}
        order by id desc;
    `);

    return [...submissions];
}

const getSubmissionByContestIdAndJunkieId = async (junkieId, contestId) => {
    const submission = await pool.query(`
        select * from submissions
        where users_id = ${junkieId} &&
        contests_id = ${contestId};
    `);

    return submission[0];
}

const getSingleContestP1 = async (contestId) => {
    const contest = await pool.query(`
        select * from phase_1
        where contests_id = ${contestId};
    `);
    return contest[0];
}

const getContestPhase = async (contestId) => {
    const phase1 = await pool.query(`
    SELECT *, 1 as phase
    FROM phase_1
    WHERE contests_id =${contestId}
    && end_date > now();
    `)

    if (phase1[0] === undefined) {
        const phase2 = await pool.query(`
    SELECT *, 2 as phase
    FROM phase_2
    WHERE contests_id =${contestId}
    && end_date > now();`)

        if (phase2[0] === undefined) {
            const phase2 = await pool.query(`
        SELECT *, 3 as phase
        FROM phase_2
        WHERE contests_id =${contestId};`)
            return phase2[0]
        }
        return phase2[0]
    }
    return phase1[0]
}

export default {
    createContest,
    activeContestsByOrganizer,
    allContestsByOrganizer,
    inactiveContestsByOrganizer,
    getContestById,
    getContests,
    activeContestsByJunkie,
    finishedContestsByJunkie,
    contestsPhase1,
    contestsPhase2,
    contestsFinished,
    createPhase1,
    createPhase2,
    createJury,
    submission,
    getJunkieScores,
    getAllOtherJunkieScores,
    certainSubmissionScore,
    getFirstPlace,
    getSecondPlace,
    getThirdPlace,
    sharedThirdPoints,
    thirdPoints,
    sharedSecondPoints,
    secondPoints,
    sharedFirstPoints,
    firstBigPoints,
    firstPoints,
    titleExists,
    invitedUsers,
    getSubmissionsByJunkieId,
    getSubmissionByContestIdAndJunkieId,
    getSingleContestP1,
    getContestPhase
}