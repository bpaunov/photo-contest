import { pool } from '../config.js';

const participationExists = async (junkieId, contestId) => {
    const participation = await pool.query(`
        select * from participations
        where users_id = ${junkieId} && 
        contests_id = ${contestId}
    `);
    return participation[0];
}

const participate = async (junkieId, contestId) => {
    const participation = await pool.query(`
        insert into participations
        (users_id, contests_id, is_deleted, date_joined)
        values
        (${junkieId}, ${contestId}, false, now());
        select * from participations
        where id = (select Max(id) from participations);
        update users
        set points = (select points from users where id = ${junkieId}) + 1
	    where id = ${junkieId};
    `);
    return participation[1][0];
}

export default {
    participationExists,
    participate
}