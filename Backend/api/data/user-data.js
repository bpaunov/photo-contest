import { pool } from '../config.js';

const getUserByUsername = async (username) => {
    const currentUserData = await pool.query(`
        select * from users
        where username = '${username}'
        `)
    return currentUserData[0];
}

const createUser = async (userInfo) => {
    await pool.query(`
    INSERT INTO users
    (username, password, first_name, last_name, is_deleted, points, date_registered)
    VALUES ('${userInfo.username}', '${userInfo.password}', '${userInfo.first_name}', '${userInfo.last_name}', false, 0, NOW())
    `)
    const user = await pool.query(`
    select *
    FROM users
    WHERE username = '${userInfo.username}'
    `)
    return user[0]
7
}

const getJunkies = async () => {
    const junkies = await pool.query(`
    SELECT *
    FROM users
    WHERE points < 51
    && is_deleted = false
    ORDER BY points DESC;
    `);
    return junkies;
}
const getEnthusiasts = async () => {
    const junkies = await pool.query(`
    SELECT *
    FROM users
    WHERE points < 151
    && is_deleted = false
    ORDER BY points DESC;
    `);
    return junkies;
}

const getMasters = async () => {
    const junkies = await pool.query(`
    SELECT *
    FROM users
    WHERE points < 1001
    && is_deleted = false
    ORDER BY points DESC;
    `);
    return junkies;
}

const getWiseguys = async () => {
    const junkies = await pool.query(`
    SELECT *
    FROM users
    WHERE points > 1000
    && is_deleted = false
    ORDER BY points DESC;
    `);
    return junkies;
}

const getCurrentPoints = async (id) => {
    const points = await pool.query(`
    SELECT points
    FROM  users
    WHERE id = ${id};
    `)
    return points[0];
}
const getJunkiesByRank = async () => {
    const users = await pool.query(`
    SELECT *
    FROM USERS
    where is_organizer = 0
    ORDER BY points DESC;
    `);
    return [...users]
}
export default {
    getUserByUsername,
    createUser,
    getCurrentPoints,
    getJunkies,
    getEnthusiasts,
    getMasters,
    getWiseguys,
    getJunkiesByRank
}