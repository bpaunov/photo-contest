import mariadb from 'mariadb';
import pkg from 'redis';
const { createClient } = pkg;

export const pool = mariadb.createPool({
    host: 'localhost',
    database: 'photo_contest',
    user: 'root',
    password: 'mariadbac' /* && '0000'*/,
    port: '3306',
    multipleStatements: true
});

export const poolBlacklist = mariadb.createPool({
    host: 'localhost',
    database: 'blacklist_tokens_library',
    user: 'root',
    password: 'mariadbac'/* && '0000'*/,
    port: '3306',
    multipleStatements: true
})

export const roles = {
    ADMIN: 'Admin',
    ORGANIZER: 'Organizer',
    COMMON: {
        JUNKIE: 'Junkie',
        ENTHUSIAST: 'Enthusiast',
        MASTER: 'Master',
        WBPD: 'Wise and Benevolent Photo Dictator'
    }
}

export const types = {
    OPEN: 'Open',
    INVITATIONAL: 'Invitational'
}
export const PORT = 3000;

export const PRIVATE_KEY = 'mega_taen_ne64upen_klu4';

export const TOKEN_LIFETIME = 60 * 60;

export const redisClient = createClient();