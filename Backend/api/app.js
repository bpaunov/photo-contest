import { PORT } from './config.js';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import sessionController from './Controllers/session-controller.js';
import junkieController from './Controllers/junkie-controller.js';
import organizerController from './Controllers/organizer-controller.js';
import juryController from './controllers/jury-controller.js';

const app = express();

passport.use(jwtStrategy);

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(passport.initialize());
app.use('/public', express.static('../../photos_of_photo_contest'));

app.use('/junkies', junkieController);
app.use('/organizers', organizerController);
app.use('/sessions', sessionController);
app.use('/jury', juryController);

app.listen(PORT, () => console.log('listening'));
