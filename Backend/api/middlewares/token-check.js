import { redisClient } from '../config.js';

export const authentication = async (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1];
    redisClient.HGET('blacklistedTokens', token, (err, reply) => {
        reply ? res.status(403).send({ message: 'Unauthorized' }) : next();
    });
}