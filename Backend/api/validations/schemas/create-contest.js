import types from '../../config.js';
export const createContestSchema = {
    category: value => {
        if (!value) {
            return 'Category name is required';
        }

        if (typeof value !== 'string' || value.length < 3 || value.length > 100) {
            return 'Category name should be a string in range [3..100] characters.';
        }

        return null;
    },
    title: value => {
        if (!value) {
            return 'Contest title is required';
        }

        if (typeof value !== 'string' || value.length < 3 || value.length > 100) {
            return 'Contest title should be a string in range [3..100] characters.';
        }

        return null;
    },
    type: value => {
        if (!value) {
            return 'Selecting contest type is required.';
        }

        if (value !== types.OPEN || value !==type.INVITATIONAL) {
            return 'Select "Open" or "Invitational" as contest type';
        }

        return null;
    },
    phase1_limit: value => {
        if (!value) {
            return 'Time limit for phase1 (days) is required.';
        }

        if (typeof value !== 'number' || Number.isNan(value) || value < 1 || value > 31) {
            return 'Phase 1 limit should be a number (days) between 1 and 31';
        }

        return null;
    },
    phase2_limit: value => {
        if (!value) {
            return 'Time limit for phase2 (hours) is required.';
        }

        if (typeof value !== 'number' || Number.isNan(value) || value < 1 || value > 24) {
            return 'Phase 2 limit should be a number (hours) between 1 and 24';
        }

        return null;
    },
    jury: value => {

        if (value){
            
            if(!(value instanceof Array)){
                return 'Please select correct numbers user IDs that you wish to be invited as jury.';
            }
            if(!value.some(isNaN)){
                return 'Please select correct numbers user IDs that you wish to be invited as jury.';
            }
            return null;
        }
    },
}