export const registerSchema = {
    username: value => {
        if (!value) {
            return 'Username is required';
        }
        if (typeof value !== 'string' || value.length < 3 || value.length > 15) {
            return 'Username should be a string in range [3..15] characters.';
        }
        return null;
    },
    password: value => {
        if (!value) {
            return 'Password is required';
        }
        if (value.length < 8){
            return 'Password should be at least 8 characters long.'
        }
        if (value.length > 50){
            return 'Password can be a maximum of 50 characters.'
        }
        const lowerCase = /[a-z\s]+/
        const upperCase = /[A-Z\s]+/
        const specialCase = /[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/
        if (lowerCase.test(value)){
            return 'Password must include at least 1 upper case character'
        }
        if (upperCase.test(value)){
            return 'Password must include at least 1 lower case character'
        }
        if (specialCase.test(value)){
            return 'Password must include at least 1 special character'
        }
    },
    first_name: value => {
        if (!value) {
            return 'First name is required';
        }
        if (typeof value !== 'string' || value.length < 3 || value.length > 30) {
            return 'First name should be a string in range [3..30] characters.';
        }
        return null;
    },
    last_name: value => {
        if (!value) {
            return 'Last name is required';
        }
        if (typeof value !== 'string' || value.length < 3 || value.length > 30) {
            return 'Last name should be a string in range [3..30] characters.';
        }
        return null;
    },
}