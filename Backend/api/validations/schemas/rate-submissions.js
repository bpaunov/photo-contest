import types from '../../config.js';
export const rateSubmission = {
    score: value => {
        if (!value) {
            return 'Category name is required';
        }

        if (typeof value !== 'number' || value < 1 || value > 10) {
            return 'Score should be between 1 and 10;';
        }

        return null;
    },
    comment: value => {
        if (!value) {
            return 'Contest title is required';
        }

        if (typeof value !== 'string' || value.length < 3 || value.length > 100) {
            return 'Submission comment should be a string in range [3..100] characters.';
        }

        return null;
    }
}