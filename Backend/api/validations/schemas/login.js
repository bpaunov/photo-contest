export const loginSchema = {
    username: value => {
        if (!value) {
            return 'Missing username';
        }
        if (typeof value !== 'string') {
            return 'Incorrect username. Please enter a string.';
        }
        return null;
    },
    password: value => {
        if (!value) {
            return 'Missing password';
        }
        return null;
    }
}