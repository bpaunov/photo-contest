import axios from 'axios';


const BASE_URL = 'http://localhost:3000/organizers';
const BASE_URL2 = 'http://localhost:3000/jury';

const orgService = {
    getPhase1(){
        return axios.get(`${BASE_URL}/contests/phase1`, { headers: { Authorization: 'Bearer ' + localStorage.getItem('token') } })
    },
    getPhase2(){
        return axios.get(`${BASE_URL}/contests/phase2`, { headers: { Authorization: 'Bearer ' + localStorage.getItem('token') } })
    },
    getFinished(){
        return axios.get(`${BASE_URL}/contests/finished`, { headers: { Authorization: 'Bearer ' + localStorage.getItem('token') } })
    },
    getJunkies(){
        return axios.get(`${BASE_URL}/junkies`, { headers: { Authorization: 'Bearer ' + localStorage.getItem('token') } })
    },
    createContest(title, category, phase1_limit, phase2_limit){

        return axios.post(`${BASE_URL}/contests`, {
            title,
            category,
            phase1_limit,
            phase2_limit
        },
        { headers: { Authorization: 'Bearer ' + localStorage.getItem('token') } })
    },
    // getJury(){

    // },
    // getSubmissionsPhase1(contestId){
    //     return axios.get(`${BASE_URL2}/contests/${contestId}/submissions-phase1`, { headers: { Authorization: 'Bearer ' + localStorage.getItem('token') } })
    // },
    // getSubmissionsPhase2(contestId){
    //     return axios.get(`${BASE_URL2}/contests/${contestId}/submissions-phase2`, { headers: { Authorization: 'Bearer ' + localStorage.getItem('token') } })
    // },
    getSubmissions(contestId){
        return axios.get(`${BASE_URL2}/contests/${contestId}/submissions`, { headers: { Authorization: 'Bearer ' + localStorage.getItem('token') } })
    },
    rateSubmission(score,not_fit,comment,submissions_id){
        return axios.post(`${BASE_URL2}/score`, {
            score,
            not_fit,
            comment,
            submissions_id
        },
        { headers: { Authorization: 'Bearer ' + localStorage.getItem('token') } })
    },
    getPhase(contestId){
        return axios.get(`${BASE_URL2}/contests/${contestId}/phase`, { headers: { Authorization: 'Bearer ' + localStorage.getItem('token') } })
    },
    
}

export default orgService;