import axios from 'axios';

const BASE_URL = 'http://localhost:3000';

const authService = {
    login(username, password) {
        return axios.post(`${BASE_URL}/sessions`, { username, password });
    },

    logout() {
        return axios.delete(`${BASE_URL}/sessions`, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        });
    },

    getOpenContestsPhase1() {
        return axios.get(`${BASE_URL}/junkies/contests/phase1`, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
    },

    getContestById(contestId) {
        return axios.get(`${BASE_URL}/junkies/contests/${contestId}`, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
    },

    getParticipatingContests() {
        return axios.get(`${BASE_URL}/junkies/contests/participations`, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
    },

    Register(username, password, first_name, last_name) {
        return axios.post(`${BASE_URL}/junkies`, {
            username,
            password,
            first_name,
            last_name
        })
    },

    participate(contestId) {
        return axios.post(`${BASE_URL}/junkies/contests/${contestId}/participate`, {}, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
    },

    submit(contestId, formData) {
        return axios.post(`${BASE_URL}/junkies/contests/${contestId}/submit`, formData, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-type': '​multipart/form-data'
            }
        })
    },

    getSubmissionByContestId(contestId) {
        return axios.get(`${BASE_URL}/junkies/contests/${contestId}/submission`, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
    },

    getSubmissionsByJunkieId(junkieId) {
        return axios.get(`${BASE_URL}/junkies/submissions/?junkieId=${junkieId}`, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
    },

    getSingleContestP1(contestId) {
        return axios.get(`${BASE_URL}/junkies/contests/${contestId}/phase1`, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
    },

    getSubmissionScoreAndComments(submissionId) {
        return axios.get(`${BASE_URL}/junkies/submissions/${submissionId}/scores`, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
    },
}

export default authService;