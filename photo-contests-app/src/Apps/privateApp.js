import React, { Fragment, useContext } from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import Header from '../Components/Header/Header';
import JunkieHomepage from '../Components/home/JunkieHomePage';
import AuthContext from '../Contexts/AuthContext.js';
import decode from 'jwt-decode'
import Contest from '../Components/JunkieComp/Contests/IndividualOpenContest';
import PartOfContest from '../Components/JunkieComp/Contests/IndividualParticipateContest.js'
import SubmissionForm from '../Components/JunkieComp/Submission/SubmissionForm';
import AllSubmissionsView from '../Components/Organizers/Submissions';
import GetPhase2 from '../Components/Organizers/Phase2';
import GetPhase1 from '../Components/Organizers/Phase1.js';
import Finished from '../Components/Organizers/Finished.js';
import GetSubmissions from '../Components/Organizers/Submissions.js';
import ContestForm from '../Components/Organizers/CreateContestForm.js';
import OpenContests from '../Components/JunkieComp/Contests/openContests';
import PartInContests from '../Components/JunkieComp/Contests/participatingIn';
import Score from '../Components/JunkieComp/Submission/SubmissionScore';
import GetJunkies from '../Components/Organizers/Junkies';
import OrgHeader from '../Components/Header/OrgHeader';
import JunkieHeader from '../Components/Header/JunkieHeader';
import Homepage from '../Components/home/Homepage';
import OrgHomepage from '../Components/home/OrgHomePage';

const PrivateApp = (props) => {
    const { user } = useContext(AuthContext);
    const info = decode(user);
    return (
        <Fragment>
            {info.role === 1 ?
                <BrowserRouter>
                    <Switch>
                        <Redirect path="/" exact to="/dashboard" />
                        <Route path="/dashboard" exact component={OrgHomepage} />
                        <Redirect path="/home" exact to='/dashboard' />
                        {/*<Redirect path="/" exact to="/home" />
                        <Route path="/dashboard" exact component={Homepage} />
            <Route path="/home" exact component={OrgHomepage} />*/}

                        <Route path="/contests/:contestId/submissions" exact component={GetSubmissions} />
                        <Route path="/contests/create" exact component={ContestForm} />
                        <Route path="/ranking" exact component={GetJunkies} />
                        <Route path="/contests/phase1" exact component={GetPhase1} />
                        <Route path="/contests/phase2" exact component={GetPhase2} />
                        <Route path="/contests/finished" exact component={Finished} />
                    </Switch>
                </BrowserRouter> :
                <BrowserRouter>
                    <Switch>
                        <Redirect path="/" exact to="/dashboard" />
                        <Redirect path="/home" exact to="/dashboard" />
                        <Route path="/dashboard" exact component={JunkieHomepage} />
                        <Route path="/contests/open" exact component={OpenContests} />
                        <Route path="/contests/participating" exact component={PartInContests} />
                        <Route path="/contests/participating/:contestId/:isFinished" exact component={PartOfContest} />
                        <Route path="/contests/:contestId/:isPartIn/submission" exact component={SubmissionForm} />
                        <Route path="/contests/:contestId" exact component={Contest} />
                        <Route path="/contests/:contestId/submission/:submissionId/score" exact component={Score} />
                    </Switch>
                </BrowserRouter>}
        </Fragment>)
}

export default PrivateApp;