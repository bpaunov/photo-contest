import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import Header from '../Components/Header/Header';
import PublicHomepage from '../Components/home/publicHomePage';
import Register from '../Components/Register/Register';

const PublicApp = props => {
    return (
        <div>
            <Header props={props} />
            <BrowserRouter>
                <Switch>
                    <Route path="/home" exact component={PublicHomepage} />
                    <Route path="/register" component={Register} />

                    <Redirect path="/" exact to="/home" />
                    <Redirect path="/dashboard" exact to="/home" />
                </Switch>
            </BrowserRouter>
        </div>
    )
}

export default PublicApp;