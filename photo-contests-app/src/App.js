import React, { Fragment, useState } from 'react';
import './App.css';
import decode from 'jwt-decode';
import PrivateApp from './Apps/privateApp';
import AuthContext from './Contexts/AuthContext';
import AppHightContext from './Contexts/AppHightContext.js'
import PublicApp from './Apps/publicApp';
import 'bootstrap/dist/css/bootstrap.min.css';
import OrgContext from './Contexts/OrgContext';
function App(props) {
  let token = localStorage.getItem('token')
  const [user, setUser] = useState(
    token && new Date() < decode(token).exp * 1000 ?
      token :
      () => {
        localStorage.removeItem('token');
        return false;
      });
  const [hight, setHight] = useState(null);
  const [CONTEST_ID, SET_CONTEST_ID] = useState(null);
  return (
    <div className='app' style={hight ? { ...hight } : null}>
      <OrgContext.Provider value={{ CONTEST_ID:CONTEST_ID, SET_CONTEST_ID: SET_CONTEST_ID}}>
      <AppHightContext.Provider value={{ hight: hight, setHight: setHight }}>
        <AuthContext.Provider value={{ user: user, setUser: setUser }}>
          {user ? <PrivateApp /> : <PublicApp />}
        </AuthContext.Provider>
      </AppHightContext.Provider>
      </OrgContext.Provider>
    </div>
  );
}

export default App;
