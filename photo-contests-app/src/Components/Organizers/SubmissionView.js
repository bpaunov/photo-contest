import Loader from '../Loader/Loader.js';
import Error from '../Error/ErrorComp.js';
import React, { useState, useEffect } from 'react';
import AuthContext from '../../Contexts/AuthContext.js';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl, Card, ListGroup, ListGroupItem } from 'react-bootstrap';
import Rate from './RateSubmissions.js';
import orgService from '../../service/orgService.js';
import moment from 'moment'

const SubmissionView = ({submission, props}) => {

    const [showRate, setShowRate] = useState(false);
    const [phase, setPhase] = useState(null);

    const ShowRate = (ev) => {
        ev.preventDefault();
        if(showRate){
            setShowRate(false);
        }
        setShowRate(true);
    }
    useEffect(() => {
        orgService.getPhase(+submission.contests_id)    
        .then(res => {
            console.log(res)
            if (res.message) {
                throw new Error(`${res.message}`);
            } else {
                setPhase(res.data);
            }
        })
        
    }, []);



    return (    
                
        <div >
            
        <Card style={{ width: '19rem' , height:'auto', margin:'10px'}} bg="light-blue" className='contest-view-class'>
  <Card.Img variant="top" src={`http://localhost:3000/public/${submission.img}`} />
    <Card.Title>{submission.story}</Card.Title>
    
  <ListGroup className="list-group-flush">
      
  <ListGroupItem>Submitted on: {moment(new Date(submission.submitted_on)).format('111')}</ListGroupItem>
    <ListGroupItem>Average score: {submission.average_score}</ListGroupItem>
  </ListGroup>
  <Card.Header id='view-subs'>{phase && phase.phase === 2 ? <Button type='button' onClick={ShowRate} variant="outline-info">Rate Submission</Button> : null}</Card.Header>
    </Card>
    <hr></hr>
                {showRate && <Rate submission={submission} props={props} /> }
    </div>
    )
    
}


export default SubmissionView;