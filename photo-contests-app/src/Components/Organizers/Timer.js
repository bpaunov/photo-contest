/* eslint-disable react-hooks/exhaustive-deps */
import React, { Fragment, useEffect, useState } from 'react';
import { diffBetweenDates } from '../../additionalFuncs/diffBetweenDates.js';
import orgService from '../../service/orgService.js';

const Timer = ({ endDate, props }) => {
    const [remainingTime, setRemainingTime] = useState({
        days: '00',
        hours: '00',
        minutes: '00',
        seconds: '00'
    });
    useEffect(() => {
        if ((remainingTime.days === 0 && remainingTime.hours === 0
            && remainingTime.minutes === 0 && remainingTime.seconds === 0)
            || remainingTime === 'Closed') {
            setRemainingTime('Closed');
        }
        else {
            setTimeout(() => setRemainingTime(diffBetweenDates(endDate, new Date())), 1000);
        }
    }, [remainingTime]);


    return (
        <Fragment>
            {typeof remainingTime === 'string' ?
                <span>Closed.</span> :
                <span>time remaining:
                {`${remainingTime.days}`.length === 1 ? ` 0${remainingTime.days}:` : ` ${remainingTime.days}:`}
                    {`${remainingTime.hours}`.length === 1 ? `0${remainingTime.hours}:` : `${remainingTime.hours}:`}
                    {`${remainingTime.minutes}`.length === 1 ? `0${remainingTime.minutes}:` : `${remainingTime.minutes}:`}
                    {`${remainingTime.seconds}`.length === 1 ? `0${remainingTime.seconds}` : `${remainingTime.seconds}`}
                </span>}
                
        </Fragment>
    )
}

export default Timer;