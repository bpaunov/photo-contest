import React, { Fragment, useState } from 'react';
import Error from '../Error/ErrorComp';
import Loader from '../Loader/Loader';
import Phase1 from './Phase1.js';
import Phase2 from './Phase2.js';
import Finished from './Finished.js';
import GetPhase1 from './Phase1.js';
import GetPhase2 from './Phase2.js';
import GetJunkies from './Junkies';
import ContestForm from './CreateContestForm';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';

const OrganizerContestsView = ({ props }) => {
    const [retrieve, setRetrieve] = useState(false);
    const [err, setError] = useState(false);


    const [showPhase1, setShowPhase1] = useState(false);
    const [showPhase2, setShowPhase2] = useState(false);
    const [showFinished, setShowFinished] = useState(false);
    const [showRanking, setShowRanking] = useState(false);

    // const showCr = (ev) => {
    //     setShowPhase1(false);
    //     setShowPhase2(false);
    //     setShowFinished(false);
    //     setShowRanking(false);
    //     setShowCreate(true);
    //     props.history.push('/contests/create')
    // }

    const showP1 = (ev) => {
        ev.preventDefault();
        setShowPhase1(true);
        setShowPhase2(false);
        setShowFinished(false);
        setShowRanking(false);
    }
    const showP2 = (ev) => {
        ev.preventDefault();
        setShowPhase2(true);
        setShowFinished(false);
        setShowPhase1(false);
        setShowRanking(false);
    }
    const showF = (ev) => {
        ev.preventDefault();
        setShowFinished(true);
        setShowPhase1(false);
        setShowPhase2(false);
        setShowRanking(false);
    }

    const showRank = (ev) => {
        setShowRanking(true);
        setShowFinished(false);
        setShowPhase1(false);
        setShowPhase2(false);
    }

    if (err) {
        return (
            <Error message={err} />
        )
    }

    if (retrieve) {
        return (
            <Loader />
        )
    }
    const cancel = (ev) => {
        ev.preventDefault();
        props.history.go(0);
    }

    const Create = (ev) => {
        props.history.push('/contests/create')
    }

    return (
        <div>
            <h1>Contests</h1>
            <hr></hr>
            <h3>Organize a New contest</h3>
            <Button type='submit' onClick={Create} variant="outline-success">Create</Button>
            <hr></hr>
            <h3>View existing contests</h3>
            <Button type='button' onClick={showP1} variant="outline-success" >Phase 1</Button>
            <Button type='button' onClick={showP2} variant="outline-success">Phase 2</Button>
            <Button type='button' onClick={showF} variant="outline-success">Finished</Button>
            {showPhase1 && <Button type='button' onClick={cancel} variant="outline-danger">Cancel</Button>}
            {showPhase2 && <Button type='button' onClick={cancel} variant="outline-danger">Cancel</Button>}
            {showFinished && <Button type='button' onClick={cancel} variant="outline-danger">Cancel</Button>}
            <hr></hr>
            <h3>Ranking</h3>
            <Button type='submit' onClick={showRank} variant="outline-success">Ranking</Button>
            {showRanking && <Button type='button' onClick={cancel} variant="outline-danger">Cancel</Button>}

            <hr></hr>
            {showPhase1 && <GetPhase1 props={props} />}
            {showPhase2 && <GetPhase2 props={props} />}
            {showFinished && <Finished props={props} />}
            {showRanking && <GetJunkies props={props} />}
            
        </div>
    
    );
}

export default OrganizerContestsView;