import React from 'react';
import {Button} from 'react-bootstrap';

const GoBackToDash = ({props}) => {
    return (
        <Button type='button' onClick={()=> {props.history.push('/dashboard')}} variant="outline-danger">Go back</Button>
    )
}

export default GoBackToDash;
