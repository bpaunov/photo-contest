import Loader from '../Loader/Loader.js';
import Error from '../Error/ErrorComp.js';
import React, { useState, useEffect } from 'react';
import orgService from '../../service/orgService.js';
import JunkieView from './JunkieView.js';
import GoBackToDash from './GoBackToDash.js';
import './ContestsView.css';

const GetJunkies = (props) => {
    const [junkies, setJunkies] = useState(false);
    const [err, setError] = useState(false);
    const [retrieve, setRetrieve] = useState(false);
    useEffect(() => {
        orgService.getJunkies()
        .then(data => {
            if(data.message) {
                throw new Error(`${data.message}`);
            } else {
                setJunkies(data.data)
            }
        }).catch((err) => setError(false))
        .finally(() => setRetrieve(false));
    }, []);

    if (retrieve) {
        return (
            <div>
                <Loader />
            </div>
        )
    }
    if (err) {
        return (
            <div>
                <Error message={err} />
            </div>
        )
    }
    return (
        <div id='contest-ios'>
            {junkies && junkies.map(el => <JunkieView key={el.id} junkie={el} /> )}
        </div>
    )
}
export default GetJunkies;