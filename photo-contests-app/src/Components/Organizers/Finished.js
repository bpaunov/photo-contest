import Loader from '../Loader/Loader.js';
import Error from '../Error/ErrorComp.js';
import React, { useState, useEffect } from 'react';
import orgService from '../../service/orgService.js';
import ContestView from './ContestsView.js';
import GoBackToDash from './GoBackToDash.js';
import './ContestsView.css';

const Finished = (props) => {
    const [contests, setContests] = useState(false);
    const [err, setError] = useState(false);
    const [retrieve, setRetrieve] = useState(false);
    useEffect(() => {
        orgService.getFinished()
        .then(data => {
            if(data.message) {
                throw new Error(`${data.message}`);
            } else {
                setContests(data.data)
            }
        }).catch((err) => setError(false))
        .finally(() => setRetrieve(false));
    }, []);

    if (retrieve) {
        return (
            <div>
                <Loader />
            </div>
        )
    }
    if (err) {
        return (
            <div>
                <Error message={err} />
            </div>
        )
    }
    return (
        <div id='contest-ios'>
            {/* <GoBackToDash props={props} /> */}
                {contests && contests.map(el => <ContestView key={el.id} contest={el}  props={props}/>)}
                {contests.length < 1 &&  <div><h3>There are no finished contests currently.</h3></div>}
        </div>
    )
}
export default Finished;