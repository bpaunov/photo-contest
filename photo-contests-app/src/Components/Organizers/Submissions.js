import Loader from '../Loader/Loader.js';
import Error from '../Error/ErrorComp.js';
import React, { useState, useEffect } from 'react';
import AuthContext from '../../Contexts/AuthContext.js';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';
import orgService from '../../service/orgService.js';
import SubmissionView from './SubmissionView.js';

const GetSubmissions = (props) => {
    const [err, setError] = useState(false);
    const [retrieve, setRetrieve] = useState(false);
    const [submissions, setSubmissions] = useState([]);

    let contestId = props.match.params.contestId;

    useEffect(() => {
        orgService.getSubmissions(contestId)
        .then(data => {
            if(data.message) {
                throw new Error(`${data.message}`);
            } else {
                
                setSubmissions([...data.data])
            }
        }).catch((err) => setError(false))
        .finally(() => setRetrieve(false));
    }, []);


    return (

        <div id='contest-ios'>
        {submissions && submissions.map(submission => <SubmissionView key={submission.id} submission={submission} props={props}/> )}
        {submissions.length === 0 &&  <p>No submissions to display.</p> }
        </div>
        
    )
}

export default GetSubmissions;