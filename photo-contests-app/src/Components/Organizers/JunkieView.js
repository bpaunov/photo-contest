import Loader from '../Loader/Loader.js';
import Error from '../Error/ErrorComp.js';
import React, { useState } from 'react';
import AuthContext from '../../Contexts/AuthContext.js';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl, Card, ListGroupItem, ListGroup  } from 'react-bootstrap';
import user from '../../Images/user.jpg';
import avatar2 from '../../Images/avatar2.jpg';
// import './CardView.css';
import moment from 'moment';

const JunkieView = ({junkie}) => {
    let role = '';
    if(junkie.points <= 50) {
        role = 'Junkie'
    }else if(junkie.points <= 150) {
        role = 'Enthusiast'
    }else if(junkie.points <= 1000) {
        role = 'Master'
    }else if(junkie.points > 1000) {
        role = 'Wise and Benevolent Photo Dictator'
    }
    return (
      <div id='contest-card'>
        <Card id={junkie.id} style={{ width: '19rem' , height:'auto'}} bg="light-blue" className='contest-view-class'>
        <Card.Img variant="top" src={avatar2} />
        <Card.Body>
          <Card.Title>Username: {junkie.username}</Card.Title>
        </Card.Body>
        <ListGroup className="list-group-flush">
          <ListGroupItem>Points: {junkie.points} - {role}</ListGroupItem>
          <ListGroupItem>First name: {junkie.first_name}</ListGroupItem>
          <ListGroupItem>Last name: {junkie.last_name}</ListGroupItem>
          <ListGroupItem>Date of registration: {moment(new Date(junkie.date_registered)).format('lll')}</ListGroupItem>
        </ListGroup>
      </Card>
      </div>
    )
    
}

export default JunkieView;