import Loader from '../Loader/Loader.js';
import Error from '../Error/ErrorComp.js';
import React, { useState, useEffect} from 'react';
import AuthContext from '../../Contexts/AuthContext.js';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';
import Select from 'react-select'
import orgService from '../../service/orgService.js'




const Score = ({submission, props}) => {

    const [selectedOption, setSelectedOption] = useState(3);
    const [notFit, setNotFit] = useState(0);

    const handleOptionChange = (changeEvent) => {

        setSelectedOption(changeEvent.target.value)
    }
    
const handleFormSubmit= (formSubmitEvent) => {
    formSubmitEvent.preventDefault();
  }

  const handleWrongCategory = (ev) => {
      setNotFit(1);
  }
  const checkRadio =(ev)=> {
      ev.preventDefault()

  }
    return (
        <div style={{marginLeft:'20px'}}>
        <div className="check-box">
        <label>
        <input type="checkbox" value="0" 
                          onChange={handleWrongCategory} />Wrong Category</label>
        </div>
        
        {/* <form onSubmit={handleFormSubmit}>
        <div className="radio">
          <label>
            <input type="radio" value="1" disabled={notFit}
                          checked={selectedOption === "1"} 
                          onChange={handleOptionChange} />1</label>
        </div>
        <div className="radio">
          <label>
            <input type="radio" value="2" disabled={notFit}
                          checked={selectedOption === "2"} 
                          onChange={handleOptionChange} />2</label>
        </div>
        <div className="radio">
          <label>
            <input type="radio" value="3" disabled={notFit}
                          checked={selectedOption === "3"} 
                          onChange={handleOptionChange} />3</label>
        </div>
        <div className="radio">
          <label>
            <input type="radio" value="4" disabled={notFit}
                          checked={selectedOption === "4"} 
                          onChange={handleOptionChange} />4</label>
        </div>
        <div className="radio">
          <label>
            <input type="radio" value="5" disabled={notFit}
                          checked={selectedOption === "5"} 
                          onChange={handleOptionChange} />5</label>
        </div>
        <div className="radio">
          <label>
            <input type="radio" value="6" disabled={notFit}
                          checked={selectedOption === "6"} 
                          onChange={handleOptionChange} />6</label>
        </div>
        <div className="radio">
          <label>
            <input type="radio" value="7" disabled={notFit}
                          checked={selectedOption === "7"} 
                          onChange={handleOptionChange} />7</label>
        </div>
        <div className="radio">
          <label>
            <input type="radio" value="8" disabled={notFit}
                          checked={selectedOption === "8"} 
                          onChange={handleOptionChange} />8</label>
        </div>
        <div className="radio">
          <label>
            <input type="radio" value="9" disabled={notFit}
                          checked={selectedOption === "9"} 
                          onChange={handleOptionChange} />9</label>
        </div>
        <div className="radio">
          <label>
            <input type="radio" value="10" disabled={notFit} enabled={!notFit}
                          checked={selectedOption === "10"} 
                          onChange={handleOptionChange} />10</label>
        </div>
      </form> */}
      <Form >
    {['radio'].map((type) => (
    <div key={`inline-${type}`} className="mb-3" disabled={notFit}>
      <Form.Check inline label="1" type={type} id={`inline-${type}-1`} />
      <Form.Check inline label="2" type={type} id={`inline-${type}-2`} />
      <Form.Check inline label="3" type={type} id={`inline-${type}-3`} />
      <Form.Check inline label="4" type={type} id={`inline-${type}-4`} />
      <Form.Check inline label="5" type={type} id={`inline-${type}-5`} />
      <Form.Check inline label="6" type={type} id={`inline-${type}-6`} />
      <Form.Check inline label="7" type={type} id={`inline-${type}-7`} />
      <Form.Check inline label="8" type={type} id={`inline-${type}-8`} />
      <Form.Check inline label="9" type={type} id={`inline-${type}-9`} />
      <Form.Check inline label="10" type={type} id={`inline-${type}-10`} />
    </div>
    ))}
    </Form>
      </div>
      )
}

export default Score;