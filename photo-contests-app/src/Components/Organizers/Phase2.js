import Loader from '../Loader/Loader.js';
import Error from '../Error/ErrorComp.js';
import React, { useState, useEffect } from 'react';
import orgService from '../../service/orgService.js';
import ContestView from './ContestsView.js';
import GoBackToDash from './GoBackToDash.js';

const GetPhase2 = (props) => {
    const [contests, setContests] = useState(false);
    const [err, setError] = useState(false);
    const [retrieve, setRetrieve] = useState(false);

    useEffect(() => {
        orgService.getPhase2()
        .then(data => {
            if(data.message) {
                throw new Error(`${data.message}`);
            } else {
                setContests(data.data)
            }
        }).catch((err) => setError(false))
        .finally(() => setRetrieve(false));
    }, []);
    
    if (retrieve) {
        return (
            <div>
                <Loader />
            </div>
        )
    }
    if (err) {
        return (
            <div>
                <Error message={err} />
            </div>
        )
    }
    return (
        <div id='contest-ios'>
            {/* <GoBackToDash props={props} /> */}
            {contests && contests.map(contest => <ContestView key={contest.id} contest={contest} props={props}/> )}
            {contests.length < 1 && <div><h3>There are no contests in phase 2 currently.</h3></div>}
        </div>
    )
}
export default GetPhase2;