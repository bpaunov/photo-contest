import Loader from '../Loader/Loader.js';
import Error from '../Error/ErrorComp.js';
import React, { useState, useEffect} from 'react';
import AuthContext from '../../Contexts/AuthContext.js';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl, Col } from 'react-bootstrap';
import Select from 'react-select'
import orgService from '../../service/orgService.js'
import Score from './Score.js';
import swal from 'sweetalert';

const Rate = ({submission, props}) => {
    const [err, setError] = useState(false);
    const [retrieve, setRetrieve] = useState(false);


    const [notFit, setNotFit] = useState(0);
    const [comment, setComment] = useState('');
    const [score, setScore] = useState(3);
   
    const submit = (ev) => {
        ev.preventDefault();
        orgService.rateSubmission(score, notFit, comment, submission.id)
        .then(res => {
            if (res.message) {
                throw new Error(`${res.message}`);
            } else {
                swal("Submission rated!", "You will be redirected to dashboard.", "success");
                props.history.push('/dashboard');
            }
        })
        .catch(err => alert(err));
    }

    const wrongCategory = (ev) => {
        setNotFit(1);
        if(notFit === 1){
            setNotFit(0);
        }
    }


    const cancelRate= (ev) => {
        // props.history.push(`/contests/${submission.contests_id}/submissions`);
        props.history.goBack()
    }

    const scoreFn = (ev) => {
        ev.preventDefault();
        setScore(ev.target.value)
    }

    const commentFn = (ev) => {
        ev.preventDefault();
        setComment(ev.target.value)
    }


    return (
                <div>
                    <Form onSubmit={submit}>
                    <Form.Group as='something' controlId="score-form">
                            <Form.Label column="lg" lg={2}>Score</Form.Label>
                            <Col lg={2}>
                            <Form.Control as="select" size="lg"  defaultValue={notFit ? "0" : "3"} onChange={scoreFn} disabled={notFit && true} style={{width:'400%'}}>
                            <option value="0" disabled={true}>0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            </Form.Control>
                            </Col>
                            <Form.Label column="lg" lg={2}>Comment</Form.Label>
                            <Col>
                            <Form.Control size="lg" type="text" placeholder="Comment" onChange={commentFn} value={notFit?  'Wrong category for this submission.' : null} disabled={notFit && true} />
                            </Col>
                            <br></br>
                            <Col>
                            <Form.Check type={"checkbox"} id={`default-checkbox`} label={`Wrong category`} onChange={wrongCategory} size="lg" lg={2}/>
                            </Col>
                            <br></br>
                            <div className="button-box">
                            <Button variant="outline-primary" type='submit' variant="outline-info" disabled={notFit && true} style={{marginLeft:'20px'}}>Submit</Button>
                            <Button type='button' variant="outline-danger" onClick={cancelRate} style={{marginLeft:'110px'}}>Cancel</Button>
                            </div>
                            </Form.Group>
                            <br></br>
                            <hr></hr>
                            </Form>
 
                            
                </div>
                
    )
    
}

export default Rate;