import Loader from '../Loader/Loader.js';
import Error from '../Error/ErrorComp.js';
import React, { useState, useEffect, useContext } from 'react';
import { Button, Card, ListGroup, ListGroupItem } from 'react-bootstrap';
import Timer from './Timer.js';
import orgService from '../../service/orgService.js';
import enter from '../../Images/enter.jpg';
import moment from 'moment';
import './ContestsView.css';
import OrgContext from '../../Contexts/OrgContext.js';

const ContestView = ({ contest, props }) => {

    const [err, setError] = useState(false);
    const [retrieve, setRetrieve] = useState(false);
    const [phase, setPhase] = useState(null);
    const {SET_CONTEST_ID} = useContext(OrgContext);
    useEffect(() => {
        orgService.getPhase(+contest.id)
            .then(res => {
                if (res.message) {
                    throw new Error(`${res.message}`);
                } else {
                    console.log(res.data.phase)
                    setPhase(res.data);
                }
            }).catch((err) => setError(false))
            .finally(() => setRetrieve(false));
    }, []);


    const showSubs = (ev) => {
        SET_CONTEST_ID(false);
        SET_CONTEST_ID(+contest.id);
    }

    if (retrieve) {
        return (
            <div>
                <Loader />
            </div>
        )
    }
    if (err) {
        return (
            <div>
                <Error message={err} />
            </div>
        )
    }
    return (
        <div id='contest-card2'>
            
        <Card style={{ width: '19rem' , height:'auto'}} bg="light-blue" className='contest-view-class'>
  <Card.Img variant="top" src={enter} />
  <span><br></br></span>
    <Card.Title>{contest.title}</Card.Title>
    <span><br></br></span>
  <ListGroup className="list-group-flush">
      
  <ListGroupItem>Category: {contest.category}</ListGroupItem>
    <ListGroupItem>Starts on: {moment(new Date(contest.start_date)).format('lll')}</ListGroupItem>
    <ListGroupItem>Ends on: {moment(new Date(contest.end_date)).format('lll')}</ListGroupItem>
    <ListGroupItem>Type: {contest.type}</ListGroupItem>
    {phase ? phase.phase === 3 ?<ListGroupItem>Contest closed.</ListGroupItem> : <ListGroupItem><Timer props={props} endDate={phase.end_date}/></ListGroupItem> : null}
  </ListGroup>
  <span><br></br></span>
  <Card.Header id='view-subs'><Button onClick={showSubs} variant="outline-info">View Submissions</Button></Card.Header>

    </Card>
    </div>
    )    
}

export default ContestView;
