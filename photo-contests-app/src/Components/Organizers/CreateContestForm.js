import React, { useState } from 'react';
import orgService from '../../service/orgService';
import { Form, Button, Col } from 'react-bootstrap';
import GoBackToDash from './GoBackToDash.js';
import swal from 'sweetalert';

const ContestForm = ({props}) => {

    const [title, setTitle] = useState('');
    const [category, setCategory] = useState('');
    const [p1, setP1] = useState(1);
    const [p2, setP2] = useState(1);

    const goBack = (ev) => {
        props.history.push('/dashboard');
    }
    const cancel = (ev) => {
        props.history.go();
    }
    const titleFn = (ev) => {
        ev.preventDefault();
        setTitle(ev.target.value);
    }
    const categoryFn = (ev) => {
        ev.preventDefault();
        setCategory(ev.target.value);
    }
    const p1Fn = (ev) => {
        ev.preventDefault();
        setP1(ev.target.value);
    }
    const p2Fn = (ev) => {
        ev.preventDefault();
        setP2(ev.target.value);
    }
    const submit = (ev) => {
        ev.preventDefault();
        orgService.createContest(title, category, p1, p2)
            .then(res => {

                if (res.message) {
                    throw new Error(`${res.message}`);
                } else {
                    swal("Contest created!", "You will be redirected to dashboard.", "success");
                }
            }).catch(err => alert(err));
    };

    return (
        <div>
            <hr></hr>
            <Form onSubmit={submit}>
                {/* <Form.Label column="lg" lg={2} style={{  marginLeft:'41.5%'}}>Title</Form.Label> */}
                <Col>
                    <Form.Control size="lg" type="text" placeholder="Title" onChange={titleFn} style={{width:'50%', margin:'auto'}} />
                </Col>
                <br></br>
                {/* <Form.Label column="lg" lg={2} style={{  marginLeft:'41.5%'}}>Category</Form.Label> */}
                <Col>
                    <Form.Control size="lg" type="text" placeholder="Category" onChange={categoryFn} style={{width:'50%', margin:'auto'}}/>
                </Col>

                <Form.Group controlId="score-form" >
                    <Form.Label column="lg" lg={2} style={{  marginLeft:'41.5%'}}>Phase 1 Limit (in days)</Form.Label>
                    <Col lg={2}>
                        <Form.Control as="select" size="lg" defaultValue={'1'} onChange={p1Fn} style={{  marginLeft:'275%'}} >
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                        </Form.Control>
                    </Col>
                    <Form.Label column="lg" lg={2} style={{  marginLeft:'41.5%'}} >Phase 2 limit (in hours)</Form.Label>
                    <Col lg={2}>
                        <Form.Control as="select" size="lg" defaultValue={1} onChange={p2Fn} style={{  marginLeft:'275%'}}>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                        </Form.Control>
                    </Col>
                    <br></br>
                    <hr></hr>
                    <Button variant="outline-info" type='submit' style={{  marginLeft:'42%'}} >Create contest</Button>
                    <Button type='button' onClick={cancel} variant="outline-danger" style={{  marginLeft:'1.5%'}}>Cancel</Button>
                </Form.Group>
            </Form>
        </div>
    )
}

export default ContestForm;