/* eslint-disable jsx-a11y/alt-text */
import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import { Carousel } from 'react-bootstrap';
import RedirectingButton from '../ButtonComp/RedirectingButton';
import './JunkieHomepageCarousel.css';
import ph1 from '../../Images/shutterstock_325323314.jpg'
import ph2 from '../../Images/shutterstock_723172429.jpg'
import ph3 from '../../Images/vZPpHJ9WoQXzvgzSom7doV.jpg'

const PublicCarousel = ({ props }) => {
    const [index, setIndex] = useState(0);

    const handleSelect = (selectedIndex, e) => {
        setIndex(selectedIndex);
    };

    return (
        <Carousel activeIndex={index} onSelect={handleSelect}>
            <Carousel.Item className="carouselItem">
                <img
                    className="d-block w-100"
                    src={ph3}
                />
                <Carousel.Caption>
                    <p id="titleTextFirstPic">First slide label</p>
                    <b id="subTitleTextFirstPic">Rediscover the world through different perspective.</b>
                    <RedirectingButton name={'Go and REGISTER NOW!'} route={`/register`} className={'caruselJoinButtonFirstPic'}
                        variant={"warning"} props={props} />
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item className="carouselItem">
                <img
                    className="d-block w-100"
                    src={ph1}
                />

                <Carousel.Caption>
                    <p id="titleTextSecPic">Save your good times</p>
                    <b id="subTitleTextSecPic">Wake up! Go and make your memories!</b>
                    <RedirectingButton name={'Go and REGISTER NOW!'} route={`/register`} className={'caruselJoinButtonSecPic'}
                        variant={'dark'} props={props} />
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item className="carouselItem">
                <img
                    className="d-block w-100"
                    src={ph2}
                />

                <Carousel.Caption>
                    <p id="titleTextThirdPic">Conquer the limits</p>
                    <b id="subTitleTextThirdPic">Rediscover the world through different perspective.</b>
                    <RedirectingButton name={'Go and REGISTER NOW!'} route={`/register`} className={'caruselJoinButtonThirdPic'}
                        variant={"danger"} props={props} />
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
    );
}

export default PublicCarousel;