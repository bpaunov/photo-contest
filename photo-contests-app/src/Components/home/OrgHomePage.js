import ControlledCarousel from '../Carousel/JunkieHomepageCarousel';
import './OrgHomePage.css';
import moment from 'moment';
import Logout from '../Session/Logout.js';
import ContestForm from '../Organizers/CreateContestForm.js';
import React, { Fragment, useState, useEffect, useContext } from 'react';
import Error from '../Error/ErrorComp';
import Loader from '../Loader/Loader';
import Phase1 from '../Organizers/Phase1.js';
import Phase2 from '../Organizers/Phase2.js';
import Finished from '../Organizers/Finished.js';
import GetPhase1 from '../Organizers/Phase1.js';
import GetPhase2 from '../Organizers/Phase2.js';
import GetJunkies from '../Organizers/Junkies';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';
import { BrowserRouter } from 'react-router-dom';
import GetSubmissions from '../Organizers/Submissions';
import SubmissionView from '../Organizers/SubmissionView.js'
import orgService from '../../service/orgService.js'
import OrgContext from '../../Contexts/OrgContext.js';

const OrgHomepage = (props) => {
  const [retrieve, setRetrieve] = useState(false);
  const [err, setError] = useState(false);


  const [showPhase1, setShowPhase1] = useState(false);
  const [showPhase2, setShowPhase2] = useState(false);
  const [showFinished, setShowFinished] = useState(false);
  const [showRanking, setShowRanking] = useState(false);
  const { CONTEST_ID, SET_CONTEST_ID } = useContext(OrgContext);
  const [submissions, setSubmissions] = useState([]);
  const [create, setShowCreate] = useState(false);
  // let contestId = props.match.params.contestId;

  useEffect(() => {
    orgService.getSubmissions(CONTEST_ID)
      .then(data => {
        if (data.message) {
          throw new Error(`${data.message}`);
        } else {

          setSubmissions([...data.data])
        }
      }).catch((err) => setError(false))
      .finally(() => setRetrieve(false));
  }, [CONTEST_ID]);


  const showCr = (ev) => {
    ev.preventDefault();
    setShowPhase1(false);
    setShowPhase2(false);
    setShowFinished(false);
    setShowRanking(false);
    setShowCreate(true);
    SET_CONTEST_ID(false);


  }

  const showP1 = (ev) => {
    ev.preventDefault();
    setShowPhase1(true);
    setShowPhase2(false);
    setShowFinished(false);
    setShowRanking(false);
    setShowCreate(false);
    SET_CONTEST_ID(false);

  }
  const showP2 = (ev) => {
    ev.preventDefault();
    setShowPhase2(true);
    setShowFinished(false);
    setShowPhase1(false);
    setShowRanking(false);
    setShowCreate(false);
    SET_CONTEST_ID(false);

  }
  const showF = (ev) => {
    ev.preventDefault();
    setShowFinished(true);
    setShowPhase1(false);
    setShowPhase2(false);
    setShowRanking(false);
    setShowCreate(false);
    SET_CONTEST_ID(false);

  }

  const showRank = (ev) => {
    ev.preventDefault();
    setShowRanking(true);
    setShowFinished(false);
    setShowPhase1(false);
    setShowPhase2(false);
    setShowCreate(false);
    SET_CONTEST_ID(false);
  }
  const showSubs = (ev) => {
    ev.preventDefault();
    setShowRanking(false);
    setShowFinished(false);
    setShowPhase1(false);
    setShowPhase2(false);
    setShowCreate(false);
  }

  if (err) {
    return (
      <Error message={err} />
    )
  }

  if (retrieve) {
    return (
      <Loader />
    )
  }
  const cancel = (ev) => {
    ev.preventDefault();
    props.history.go(0);
  }

  const Create = (ev) => {
    props.history.push('/contests/create')
  }
  return (
    <div class="grid-container">
      <div class="menu-icon"><i class="fas fa-bars header__menu"></i></div>

      <header class="header">
        <div class="header__search">Date: {moment(new Date()).format('l LTS')}</div>
        <div class="header__avatar">Welcome back</div>

      </header>

      <aside class="sidenav">
        <div class="sidenav__close-icon">
          <i class="fas fa-times sidenav__brand-close"></i>
        </div>
        <ul class="sidenav__list">

          <li class="sidenav__list-item">
            <div class="overviewcard">
              <div class="overviewcard__icon">Contests Phase 1</div>
              {!showPhase1 && <Button type='button' onClick={showP1} variant="outline-info" >Show</Button>}
              {showPhase1 && <Button type='button' onClick={() => { setShowPhase1(false); SET_CONTEST_ID(false) }} variant="outline-danger">Hide</Button>}
            </div>
          </li>

          <li class="sidenav__list-item">
            <div class="overviewcard">
              <div class="overviewcard__icon">Contests Phase 2</div>
              {!showPhase2 && <Button type='button' onClick={showP2} variant="outline-info">Show</Button>}
              {showPhase2 && <Button type='button' onClick={() => { setShowPhase2(false); SET_CONTEST_ID(false) }} variant="outline-danger">Hide</Button>}
            </div>
          </li>
          <li class="sidenav__list-item">
            <div class="overviewcard">
              <div class="overviewcard__icon">Finished contests</div>
              {!showFinished && <Button type='button' onClick={showF} variant="outline-info">Show</Button>}
              {showFinished && <Button type='button' onClick={() => { setShowFinished(false); SET_CONTEST_ID(false) }} variant="outline-danger">Hide</Button>}
            </div>
          </li>
          <li class="sidenav__list-item" ><div class="overviewcard">
            <div class="overviewcard__icon">Ranking</div>
            {!showRanking && <Button type='submit' onClick={showRank} variant="outline-info">Ranking</Button>}
            {showRanking && <Button type='button' onClick={cancel} variant="outline-danger">Hide</Button>}
          </div></li>
          <li class="sidenav__list-item" ><div class="overviewcard">
            <div class="overviewcard__icon">Create New contest</div>
            {!create && <Button type='submit' onClick={showCr} variant="outline-info">Create</Button>}
            {create && <Button type='button' onClick={cancel} variant="outline-danger">Hide</Button>}
          </div></li>
          {/* <li class="sidenav__list-item"><div class="overviewcard">
            <div class="overviewcard__icon"></div>
            <Button variant="outline-info" >Score a submission</Button>
          </div></li> */}
          <li class="sidenav__list-item"><div class="overviewcard">
            <div class="overviewcard__icon">Quit</div>
            <Logout />
          </div></li>

        </ul>
      </aside>

      <main class="main" style={{ paddingTop: '20px' }}>
        {showPhase1 && <GetPhase1 props={props} />}
        {showPhase2 && <GetPhase2 props={props} />}
        {showFinished && <Finished props={props} />}
        {showRanking && <GetJunkies props={props} />}
        {create && <div><ContestForm props={props} /></div>}
        {CONTEST_ID && <div id='contest-ios'>{submissions && submissions.map(submission => <SubmissionView key={submission.id} submission={submission} props={props} />)}
          {submissions.length === 0 && <p>No submissions to display.</p>}</div>}
        <div class="main-cards" >



        </div>
      </main>

      <footer class="footer">
        <div class="footer__signature">B&T contests</div>
      </footer>
    </div>

  )
}

export default OrgHomepage;