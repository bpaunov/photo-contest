import React from 'react';
import ControlledCarousel from '../Carousel/JunkieHomepageCarousel';
import JunkieHeader from '../Header/JunkieHeader';

const JunkieHomepage = (props) => {
    return (
        <div>
            <JunkieHeader props={props} />
            <ControlledCarousel props={props} />
        </div>
    )
}

export default JunkieHomepage;