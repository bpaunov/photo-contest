import React from 'react';
import Header from '../Header/Header.js'
import PublicCarousel from '../Carousel/publicCarousel.js';

const PublicHomepage = (props) => {
    return (
        <div>
            <PublicCarousel props={props} />
        </div>
    )
}

export default PublicHomepage;