import React from 'react';
import OrgHeader from '../Header/OrgHeader.js';
import Header from '../Header/Header'

const Homepage = (props) => {
    return (
        <div>
            <OrgHeader props={props} />
        </div>
    )
}

export default Homepage;