import React, { useContext, useState } from 'react';
import AuthContext from '../../Contexts/AuthContext.js';
import authService from '../../service/authService.js';
import Error from '../Error/ErrorComp.js';
import { Button } from 'react-bootstrap';
import AppHightContext from '../../Contexts/AppHightContext.js';

const Logout = (props) => {
    const { setHight } = useContext(AppHightContext);
    const { setUser } = useContext(AuthContext);
    const [err, setError] = useState(false);

    const logout = (ev) => {
        ev.preventDefault();
        authService.logout()
            .then(res => {
                localStorage.removeItem('token');
                setHight(null);
                setUser(false);
                props.history.push('/')
            })
            .catch(err => setError(err))
    }

    if (err) {
        return <Error message={err} />
    }

    return <Button variant="outline-info" onClick={logout}>Logout</Button>
}

export default Logout;