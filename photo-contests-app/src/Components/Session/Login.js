import React, { useContext, useState } from 'react';
import AuthContext from '../../Contexts/AuthContext.js';
import authService from '../../service/authService.js';
import Loader from '../Loader/Loader.js';
import Error from '../Error/ErrorComp.js';

const Login = props => {
    const { setUser } = useContext(AuthContext);
    const [loginData, setLoginData] = useState({ username: null, password: null });
    const [err, setError] = useState(false);
    const [retrieve, setRetrieve] = useState(false);

    const login = (ev) => {
        ev.preventDefault();
        setRetrieve(true);

        authService.login(loginData.username, loginData.password)
            .then(res => {
                if (res.message) {
                    throw new Error(`${res.message}`)
                }
                else {
                    localStorage.setItem('token', res.data.token);
                    setUser(res.data.token);
                    props.history.push('/home')
                }
            })
            .catch(err => setError(err))
            .finally(() => setRetrieve(false));
    }

    if (retrieve) {
        return (
            <div>
                <Loader />
            </div>
        )
    }

    if (err) {
        return (
            <div>
                <Error message={err} />
            </div>
        )
    }

    return (
        <div>
            <form>
                <input type='text' placeholder='Username' onChange={(e) => loginData.username = e.target.value} />
                <input type='password' placeholder='Password' onChange={(e) => loginData.password = e.target.value} />
                <button type='submit' onClick={login}>Login</button>
            </form>
        </div>
    )
}

{/* <Form>
                <Form.Group controlId="formBasicEmail">
                    <Form.Control type="text" placeholder="Username" onChange={(e) => loginData.username = e.target.value} style={{ marginRight: '4px' }} />
                    <Form.Control type="password" placeholder="Password" onChange={(e) => loginData.username = e.target.value} style={{ marginRight: '4px' }} />
                    <Button type='submit' onClick={login}>Login</Button>
                </Form.Group>
            </Form> */}

export default Login;