import React, { useState } from 'react';
import AuthContext from '../../Contexts/AuthContext.js';
import authService from '../../service/authService.js';
import Loader from '../Loader/Loader.js';
import Error from '../Error/ErrorComp.js';
import decode from 'jwt-decode';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';
import {
  CardWrapper,
  CardHeader,
  CardHeading,
  CardBody,
  CardIcon,
  CardFieldset,
  CardInput,
  CardOptionsItem,
  CardOptions,
  CardOptionsNote,
  CardButton,
  CardLink
} from "./Card";
import "./Register.css";
import swal from 'sweetalert';
import GoBackToDash from '../Organizers/GoBackToDash.js';


const Register = (props) => {
  const [err, setError] = useState(false);

  const [retrieve, setRetrieve] = useState(false);

  // const [registration, setRegistrationState] = useState(false);

  const [registrationInfo, setRegistrationInfo] = useState({
    username: null,
    password: null,
    first_name: null,
    last_name: null
  });
  const register = ({ username, password, first_name, last_name }) => {
    setRetrieve(true);
    if (!username || !password || !first_name || !last_name) {
      alert('There is missing info');
      return;
    }
    // setRegistrationState(null);
    authService.Register(username, password, first_name, last_name)
      .then(data => {
        if (data.message) {
          throw new Error(`${data.message}`);
        } else {
          setRegistrationInfo({
            username: null,
            password: null,
            first_name: null,
            last_name: null
          });
          swal("Successfully registered!", "You will be redirected to dashboard.", "success");
          props.history.push('/home')
        }
      })
      .catch((err) => setError(false))
      .finally(() => setRetrieve(false));
  }

  // if (registration) {

  //     return (
  //         <div>
  //             <h1>You are registered</h1>
  //             {props.history.push('/home')}
  //         </div>

  //     )
  // }
  if (retrieve) {
    return (
      <div>
        <Loader />
      </div>
    )
  }
  if (err) {
    return (
        // <Form>
        //     <div>
        //     <label>Username:</label>
        //     <input type='Username' placeholder='required' onChange={(e) => registrationInfo.username = e.target.value}/>
        //     </div>
        //     <div>
        //     <label>Password: </label>
        //     <input type='Password' placeholder='required' onChange={(e) => registrationInfo.password = e.target.value}/>
        //     </div>
        //     <div>
        //     <label htmlFor='First name'>First name:</label>
        //     <input type='First name' placeholder='required' onChange={(e) => registrationInfo.first_name = e.target.value}/>
        //     </div>
        //     <div>
        //     <label>Last name:</label>
        //     <input type='Last name' placeholder='required' onChange={(e) => registrationInfo.last_name = e.target.value}/>
        //     </div>
        //     <Button type='Submit' onClick={() =>  register({ ...registrationInfo })}> Create account </Button>
        // </Form >

<div className="App">
<CardWrapper>
  <CardHeader>
    <CardHeading>Register</CardHeading>
  </CardHeader>

  <CardBody>
    <CardFieldset>
      <CardInput placeholder="Username" type="text" onChange={(e) => registrationInfo.username = e.target.value} required />
    </CardFieldset>

    <CardFieldset>
      <CardInput placeholder="First name" type="text" onChange={(e) => registrationInfo.first_name = e.target.value} required />
    </CardFieldset>

    <CardFieldset>
      <CardInput placeholder="Last name" type="text" onChange={(e) => registrationInfo.last_name = e.target.value} required />
    </CardFieldset>

    <CardFieldset>
      <CardInput placeholder="Password" type="password" onChange={(e) => registrationInfo.password = e.target.value} required />
      <CardIcon className="fa fa-eye" eye small />
    </CardFieldset>

    <CardFieldset>
      <CardOptionsNote></CardOptionsNote>

      <GoBackToDash props={props} />
    </CardFieldset>

    <CardFieldset>
      <CardButton type="button" type='Submit' onClick={() =>  register({ ...registrationInfo })}>Sign Up</CardButton>
    </CardFieldset>

    <CardFieldset>
      <CardLink>I already have an account</CardLink>
    </CardFieldset>
  </CardBody>
</CardWrapper>
</div>
    )
  }
  return (
    // <Form>
    //     <div>
    //     <label>Username:</label>
    //     <input type='Username' placeholder='required' onChange={(e) => registrationInfo.username = e.target.value}/>
    //     </div>
    //     <div>
    //     <label>Password: </label>
    //     <input type='Password' placeholder='required' onChange={(e) => registrationInfo.password = e.target.value}/>
    //     </div>
    //     <div>
    //     <label htmlFor='First name'>First name:</label>
    //     <input type='First name' placeholder='required' onChange={(e) => registrationInfo.first_name = e.target.value}/>
    //     </div>
    //     <div>
    //     <label>Last name:</label>
    //     <input type='Last name' placeholder='required' onChange={(e) => registrationInfo.last_name = e.target.value}/>
    //     </div>
    //     <Button type='Submit' onClick={() =>  register({ ...registrationInfo })}> Create account </Button>
    // </Form >

    <div className="App">
      <CardWrapper>
        <CardHeader>
          <CardHeading>Register</CardHeading>
        </CardHeader>

        <CardBody>
          <CardFieldset>
            <CardInput placeholder="Username" type="text" onChange={(e) => registrationInfo.username = e.target.value} required />
          </CardFieldset>

          <CardFieldset>
            <CardInput placeholder="First name" type="text" onChange={(e) => registrationInfo.first_name = e.target.value} required />
          </CardFieldset>

          <CardFieldset>
            <CardInput placeholder="Last name" type="text" onChange={(e) => registrationInfo.last_name = e.target.value} required />
          </CardFieldset>

          <CardFieldset>
            <CardInput placeholder="Password" type="password" onChange={(e) => registrationInfo.password = e.target.value} required />
            <CardIcon className="fa fa-eye" eye small />
          </CardFieldset>

          <CardFieldset>
            <CardOptionsNote></CardOptionsNote>

            <GoBackToDash props={props} />
          </CardFieldset>

          <CardFieldset>
            <CardButton type="button" type='Submit' onClick={() => register({ ...registrationInfo })}>Sign Up</CardButton>
          </CardFieldset>

          <CardFieldset>
            <CardLink>I already have an account</CardLink>
          </CardFieldset>
        </CardBody>
      </CardWrapper>
    </div>
  )
}

export default Register;