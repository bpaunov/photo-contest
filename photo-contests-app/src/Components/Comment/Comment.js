import React from 'react';
import { ListGroup, ListGroupItem } from 'react-bootstrap';

const Comment = ({ commentData }) => {
    return (
        <ListGroup style={{ width: '60%', margin: 'auto', marginTop: '10px', marginBottom: '10px' }}>
            <ListGroupItem>Score - {commentData.score}</ListGroupItem>
            <ListGroupItem>Comment - {commentData.comment}</ListGroupItem>
            <ListGroupItem>By: {commentData.juror}</ListGroupItem>
        </ListGroup>
    )
}

export default Comment;