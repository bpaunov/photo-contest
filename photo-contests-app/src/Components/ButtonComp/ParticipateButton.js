import React from 'react';
import { Button } from 'react-bootstrap';
import authService from '../../service/authService';

const ParticipateButton = ({ contest, marginLeft, marginTop, props }) => {

    const participate = (ev) => {
        ev.preventDefault();
        authService.participate(+contest.id)
            .then(res => {
                if (res.message) {
                    throw new Error(`${res.message}`);
                } else {
                    props.history.push(`/contests/${+contest.id}/${0}/submission`);
                }
            })
            .catch(err => {
                alert(`${err}`);
                props.history.goBack();
            });
    }

    return <Button onClick={participate} style={marginLeft ? { marginLeft: marginLeft } : null}>Join</Button>
}

export default ParticipateButton;