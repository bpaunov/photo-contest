import React from 'react';
import { Button } from 'react-bootstrap';

const RedirectingButton = ({ name, route, marginLeft, className, variant, props }) => {
    const redirectTo = (ev) => {
        ev.preventDefault();
        props.history.push(route);
    }
    return <Button className={className} variant={variant || "outline-success"} onClick={redirectTo}
        style={marginLeft ? { marginLeft: marginLeft } : null}>{name}</Button>
}

export default RedirectingButton;