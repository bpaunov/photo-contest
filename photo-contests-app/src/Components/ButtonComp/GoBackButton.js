import React from 'react';
import { Button } from 'react-bootstrap';

const GoBackButton = ({ name, props }) => {
    const redirectTo = (ev) => {
        ev.preventDefault();
        props.history.goBack();
    }
    return <Button variant="outline-success" onClick={redirectTo}>{name}</Button>
}

export default GoBackButton;