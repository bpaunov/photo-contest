import React from 'react';
import { NavDropdown } from 'react-bootstrap';

const NavDropdownItem = ({ name, route, props }) => {
    const redirectTo = (ev) => {
        ev.preventDefault();
        props.history.push(route);
    }
    return <NavDropdown.Item onClick={redirectTo}>{`${name}`}</NavDropdown.Item>
}

export default NavDropdownItem;