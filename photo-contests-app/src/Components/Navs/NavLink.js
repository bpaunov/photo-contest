import React from 'react';
import { Nav } from 'react-bootstrap';

const NavLinkTo = ({ name, route, props }) => {
    const redirectTo = (ev) => {
        ev.preventDefault();
        props.history.push(route);
    }
    return <Nav.Link onClick={redirectTo}>{name}</Nav.Link>
}

export default NavLinkTo;