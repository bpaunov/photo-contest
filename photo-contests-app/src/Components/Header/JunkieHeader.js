import React from 'react';
import { Button, Form, Nav, Navbar, NavDropdown } from 'react-bootstrap';
import NavDropdownItem from '../Navs/NavDropdownItem';
import NavLinkTo from '../Navs/NavLink';
import Logout from '../Session/Logout';

const JunkieHeader = ({ props }) => {
    return (
        <div>
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand>B&T CONTESTS</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <NavLinkTo name="Home" route="/dashboard" props={props} />
                        <NavDropdown title="Contests" id="basic-nav-dropdown">
                            <NavDropdownItem name={"Open contests"} route={'/contests/open'} props={props} />
                            <NavDropdownItem name={'Part Of'} route={'/contests/participating'} props={props} />
                        </NavDropdown>
                    </Nav>
                    <Form inline>
                        <Navbar.Brand style={{ marginRight: '20px' }}>Hello, Teo!</Navbar.Brand>
                        <Logout />
                    </Form>
                </Navbar.Collapse>
            </Navbar>
        </div>
    );
}

export default JunkieHeader;