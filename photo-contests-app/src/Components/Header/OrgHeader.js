import React from 'react';
import { Button, Form, Nav, Navbar, NavDropdown } from 'react-bootstrap';
import NavDropdownItem from '../Navs/NavDropdownItem';
import NavLinkTo from '../Navs/NavLink';
import Logout from '../Session/Logout';

const OrgHeader = ({ props }) => {
    return (
        <div>
            <Navbar bg="dark" variant="dark" expand="lg">
                <Navbar.Brand href="#home">B&T CONTESTS</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <NavLinkTo name="Home" route="/dashboard" props={props} />
                        <NavDropdown title="Contests" id="basic-nav-dropdown">
                            <NavDropdownItem name={'Phase 1'} route={'/contests/phase1'} props={props} />
                            <NavDropdownItem name={'Phase 2'} route={'/contests/phase2'} props={props} />
                            <NavDropdownItem name={'Finished'} route={'/contests/finished'} props={props} />
                        </NavDropdown>
                        <NavLinkTo name="Ranking" route="/ranking" props={props} />
                        <NavLinkTo name="Create contest" route="/contests/create" props={props} />
                    </Nav>
                    <Form inline>
                        <Logout />
                    </Form>
                </Navbar.Collapse>
            </Navbar>
        </div>
    );
}

export default OrgHeader;

