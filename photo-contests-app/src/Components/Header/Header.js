import React, { useContext } from 'react';
import AuthContext from '../../Contexts/AuthContext.js';
import Login from '../Session/Login.js';
import Logout from '../Session/Logout.js';
import Register from '../Register/Register.js'
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';
import GetPhase1 from '../Organizers/Phase1.js';
import GetPhase2 from '../Organizers/Phase2.js';
import Finished from '../Organizers/Finished.js';
import ContestView from '../Organizers/ContestsView.js';
import GetJunkies from '../Organizers/Junkies.js';
import NavLinkTo from '../Navs/NavLink.js';

const Header = (props) => {
    const { user } = useContext(AuthContext);
    return (
        <div>
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand>B&T CONTESTS</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                    </Nav>
                    <Form inline>
                        <Nav.Link href="http://localhost:4000/register">Register</Nav.Link>
                        <Login />
                    </Form>
                </Navbar.Collapse>
            </Navbar>
        </div>
    )
};

export default Header;