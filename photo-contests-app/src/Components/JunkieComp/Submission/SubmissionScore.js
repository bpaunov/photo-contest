/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { Fragment, useContext, useEffect, useState } from 'react';
import AppHightContext from '../../../Contexts/AppHightContext';
import authService from '../../../service/authService';
import GoBackButton from '../../ButtonComp/GoBackButton';
import Comment from '../../Comment/Comment';
import JunkieHeader from '../../Header/JunkieHeader';
import background from '../../../Images/Trees-tunnel-road_1920x1200.jpg';
import { ListGroup, ListGroupItem } from 'react-bootstrap';

const Score = (props) => {
    const { setHight } = useContext(AppHightContext);
    const [submission, setSubmission] = useState(null)
    const [comments, setComments] = useState([]);

    useEffect(() => {
        authService.getSubmissionScoreAndComments(props.match.params.submissionId)
            .then((res) => {
                if (res.message) {
                    throw new Error(`${res.message}`);
                } else {
                    setComments([...res.data]);
                }
            })
    }, [])

    useEffect(() => {
        authService.getSubmissionByContestId(props.match.params.contestId)
            .then((res) => {
                if (res.message) {
                    throw new Error(`${res.message}`);
                } else {
                    setSubmission(res.data);
                }
            })
    }, [])

    useEffect(() => {
        setHight({
            height: 'auto',
            width: '100%',
            backgroundImage: `url(${background})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            backgroundAttachment: 'fixed',
        });
    }, []);

    return (
        <div style={{ height: '92.4%' }}>
            <JunkieHeader props={props} />
            <div id='infoContainer' style={{ height: 'auto' }}>
                <GoBackButton name={'Go Back'} props={props} />
                {submission &&
                    <Fragment>
                        <ListGroup style={{ width: '50%', margin: 'auto' }}>
                            <ListGroupItem>Your photo</ListGroupItem>
                            <ListGroupItem><img style={{ margin: 'auto' }} src={`http://localhost:3000/public/${submission.img}`} /></ListGroupItem>
                            <ListGroupItem style={{ backgroundColor: 'orange' }}>Overall rate: {submission.average_score}</ListGroupItem>
                        </ListGroup>
                    </Fragment>}
                {comments.map((comment) => <Comment key={`comment.id`} commentData={comment} />)}
            </div>
        </div>
    )
}

export default Score;