import React, { useContext, useEffect, useState } from 'react';
import authService from '../../../service/authService';
import swal from 'sweetalert';
import { Form, ListGroup, ListGroupItem } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import background from '../../../Images/pexels-pixabay-162606.jpg';
import AppHightContext from '../../../Contexts/AppHightContext';

const SubmissionForm = (props) => {
    const { setHight } = useContext(AppHightContext);
    const [isFormValid, setIsFormValid] = useState(false);
    const [formFile, setFormFile] = useState(new FormData())
    const [form, setForm] = useState({
        title: {
            placeholder: 'Photo title...',
            touched: false,
            validations: {
                required: true,
                minLength: 3,
                maxLength: 50,
            },
            valid: false,
            value: '',
        },
        story: {
            placeholder: 'Photo story...',
            touched: false,
            validations: {
                required: true,
                minLength: 1,
                maxLength: 200,
            },
            valid: false,
            value: '',
        }
    });

    useEffect(() => {
        setHight({
            height: '100vh',
            width: '100%',
            backgroundImage: `url(${background})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            backgroundAttachment: 'fixed',
        });
    }, []);

    const isInputValid = (validation, value) => {
        let isValid = true;
        if (validation.required) {
            isValid = isValid && value.length !== 0;
        }
        if (validation.minLength) {
            isValid = isValid && value.length >= validation.minLength
        }
        if (validation.maxLength) {
            isValid = isValid && value.length <= validation.maxLength
        }
        return isValid;
    }

    const handleInputChange = event => {
        const { name, value } = event.target;
        const updatedControl = { ...form[name] };

        updatedControl.value = value;
        updatedControl.touched = true;
        updatedControl.valid = isInputValid(updatedControl.validations, value);

        const updatedForm = { ...form, [name]: updatedControl };
        setForm(updatedForm);

        const formValid = Object.values(updatedForm).every(control => control.valid);
        setIsFormValid(formValid);
        formFile.delete(name);
        formFile.append(name, value);
    }

    const submit = (ev) => {
        ev.preventDefault();
        authService.submit(props.match.params.contestId, formFile)
            .then(res => {
                if (res.message) {
                    throw new Error(`${res.message}`);
                } else {
                    swal("Submission successful!", "You will be taken back", "success");
                    +props.match.params.isPartIn === 1 ?
                        props.history.replace(`/contests/participating/${+props.match.params.contestId}/${0}`) :
                        props.history.replace(`/contests/${+props.match.params.contestId}`)
                }
            })
            .catch(err => alert(err));
    }

    const cancelSubmission = (ev) => {
        ev.preventDefault();
        +props.match.params.isPartIn === 1 ?
            props.history.replace(`/contests/participating/${+props.match.params.contestId}/${0}`) :
            props.history.replace(`/contests/${+props.match.params.contestId}`)
    }

    const handleFileChange = (ev) => {
        ev.preventDefault();
        formFile.delete('photo')
        formFile.append('photo', ev.target.files[0], ev.target.files[0].name);
    };

    return (
        <div style={{ height: '100%', width: '100%', paddingTop: '100px' }}>
            <ListGroup style={{ width: '50%', margin: 'auto' }}>
                <ListGroupItem><Form onSubmit={submit}>
                    <Form.Group controlId="formBasicPassword">
                        <Form.Control type='text' style={{ width: '15%', margin: 'auto' }} name={'title'} placeholder={form.title.placeholder} value={form.title.value} onChange={handleInputChange} />
                    </Form.Group>
                    <Form.File id="exampleFormControlFile1" onChange={handleFileChange} style={{ width: '15%', margin: 'auto' }} />
                    <Form.Control as="textarea" rows={4} cols="50" name='story'
                        placeholder={form.story.placeholder} value={form.story.value} onChange={handleInputChange}
                        style={{ width: '80%', height: '30%', margin: 'auto', marginTop: '30px', marginBottom: '30px' }} />
                    <Button variant="primary" type='submit' disabled={!isFormValid}
                        style={{ width: '20%', marginLeft: '40%' }} >Submit info</Button>
                </Form>
                    <form onSubmit={cancelSubmission}>
                        <Button type='submit' variant="danger" style={{ marginLeft: '70%', marginTop: '10px' }}>Cancel</Button>
                    </form>
                </ListGroupItem>
            </ListGroup>
        </div>
    )
}


{/* <form onSubmit={submit} id='submissionForm'>
                <input type='text' name={'title'} placeholder={form.title.placeholder} value={form.title.value} onChange={handleInputChange} />
                <input type="file" onChange={handleFileChange} />
                <button variant="outline-primary" type='submit' disabled={!isFormValid}>Submit info</button>
            </form>

            <textarea rows="4" cols="50" name='story' form="submissionForm"
                placeholder={form.story.placeholder} value={form.story.value} onChange={handleInputChange} /> */}

export default SubmissionForm;