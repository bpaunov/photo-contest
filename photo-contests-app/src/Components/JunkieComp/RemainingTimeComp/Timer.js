/* eslint-disable react-hooks/exhaustive-deps */
import React, { Fragment, useEffect, useState } from 'react';
import { diffBetweenDates } from '../../../additionalFuncs/diffBetweenDates';

const Timer = ({ endDate, props }) => {
    const [remainingTime, setRemainingTime] = useState({
        days: '00',
        hours: '00',
        minutes: '00',
        seconds: '00'
    });

    useEffect(() => {
        if ((remainingTime.days === 0 && remainingTime.hours === 0
            && remainingTime.minutes === 0 && remainingTime.seconds === 0)
            || remainingTime === 'Closed') {
            setRemainingTime('Closed');
        }
        else {
            setTimeout(() => setRemainingTime(diffBetweenDates(endDate, new Date())), 1000);
        }
    }, [remainingTime]);

    return (
        <Fragment>
            {typeof remainingTime === 'string' ?
                <span>Closed for submition</span> :
                <span>time remaining:
                {`${remainingTime.days}`.length === 1 ? ` 0${remainingTime.days} Days :` : `${remainingTime.days} Days:`}
                    {`${remainingTime.hours}`.length === 1 ? `0${remainingTime.hours} Hours:` : `${remainingTime.hours} Hours:`}
                    {`${remainingTime.minutes}`.length === 1 ? `0${remainingTime.minutes} Minutes:` : `${remainingTime.minutes} Minutes :`}
                    {`${remainingTime.seconds}`.length === 1 ? `0${remainingTime.seconds} Seconds` : `${remainingTime.seconds} Seconds`}
                </span>}
        </Fragment>
    )
}

export default Timer;