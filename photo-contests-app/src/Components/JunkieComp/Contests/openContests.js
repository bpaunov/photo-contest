/* eslint-disable jsx-a11y/alt-text */
import React, { useContext, useEffect, useState } from 'react';
import AuthContext from '../../../Contexts/AuthContext.js';
import authService from '../../../service/authService.js';
import Error from '../../Error/ErrorComp.js';
import Loader from '../../Loader/Loader.js';
import decode from 'jwt-decode';
import RedirectingButton from '../../ButtonComp/RedirectingButton.js';
import JunkieHeader from '../../Header/JunkieHeader.js';
import { Card } from 'react-bootstrap';
import '../css-es/ContestsPages.css';
import contestPhoto from '../../../Images/images.jpg';
import background from '../../../Images/Trees-tunnel-road_1920x1200.jpg';
import AppHightContext from '../../../Contexts/AppHightContext.js';

const OpenContests = (props) => {
    const { setHight } = useContext(AppHightContext);
    const { user } = useContext(AuthContext);
    const [retrieve, setRetrieve] = useState(false);
    const [err, setError] = useState(false);
    const [contests, setContests] = useState(false);
    const [checkForPartIn, setCheckPartInState] = useState(false);
    const [checkForSubmissionsIn, setCheckForSubmissionsState] = useState(false);

    useEffect(() => {
        setRetrieve(true);
        authService.getOpenContestsPhase1()
            .then((res) => {
                if (res.message) {
                    throw new Error(`${res.data.message}`);
                } else {
                    setContests(res.data);
                }
            })
            .catch(err => setError(err))
            .finally(() => setRetrieve(false));
    }, [])

    useEffect(() => {
        authService.getParticipatingContests()
            .then((res) => {
                if (res.message) {
                    throw new Error(`${res.message}`);
                } else {
                    let result = new Map();
                    res.data.forEach(c => {
                        result.set(c.id, c.id)
                    });
                    setCheckPartInState(result);
                }
            })
            .catch(err => setError(err));
    }, [])

    useEffect(() => {
        authService.getSubmissionsByJunkieId(decode(user).sub)
            .then((res) => {
                if (res.message) {
                    throw new Error(`${res.message}`);
                } else {
                    let result = new Map();
                    res.data.forEach(s => {
                        result.set(s.contests_id, s.contests_id);
                    });
                    setCheckForSubmissionsState(result)
                }
            })
            .catch(err => setError(err));
    }, [])

    useEffect(() => {
        if (contests.length <= 4) {
            setHight({
                height: '100vh',
                width: '100%',
                backgroundImage: `url(${background})`,
                backgroundRepeat: 'no-repeat',
                backgroundSize: 'cover',
                backgroundAttachment: 'fixed',
            });
        }
    }, [contests]);

    if (err) {
        return <Error message={err} />
    }

    if (retrieve) {
        return <Loader />
    }

    if (contests && checkForPartIn && checkForSubmissionsIn) {
        return (
            <div id='bg' style={contests.length <= 4 ? { height: '92.4%' } : { height: 'auto' }}>
                <JunkieHeader props={props} />
                <div id='openContestsContainer' style={contests.length <= 4 ? { height: '100%' } : { height: 'auto' }}>
                    {!contests ? <p>No open contests at this moment</p> :
                        contests.map((contest) => new Date(contest.end_date) < new Date() ?
                            null : +checkForPartIn.has(contest.id) ||
                                +checkForSubmissionsIn.has(contest.id) ?
                                <div key={`contestCard${contest.id}`} id={`contestCard${contest.id}`} className='contestCard'>
                                    <Card style={{ width: '11rem', height: '350px', padding: '0px' }}>
                                        <Card.Img variant="top" src={contestPhoto} />
                                        <Card.Body>
                                            <Card.Title>{contest.title}</Card.Title>
                                            <Card.Text>Participating In</Card.Text>
                                            <RedirectingButton name={'See more'} route={`/contests/${contest.id}`} props={props} />
                                        </Card.Body>
                                    </Card>
                                </div>
                                :
                                <div key={`contestCard${contest.id}`} id={`contestCard${contest.id}`} className='contestCard'>
                                    <Card style={{ width: '12rem', height: '350px', padding: '0px' }}>
                                        <Card.Img variant="top" src={contestPhoto} />
                                        <Card.Body>
                                            <Card.Title>{contest.title}</Card.Title>
                                            <Card.Text></Card.Text>
                                            <RedirectingButton name={'See more'} route={`/contests/${contest.id}`} props={props} />
                                        </Card.Body>
                                    </Card>
                                </div>)
                    }
                </div>
            </div>
        )
    }

    return null;
}

export default OpenContests;