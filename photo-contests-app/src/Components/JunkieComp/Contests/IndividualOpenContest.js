/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect, useState } from 'react';
import authService from '../../../service/authService.js';
import Error from '../../Error/ErrorComp.js';
import Loader from '../../Loader/Loader.js';
import RedirectingButton from '../../ButtonComp/RedirectingButton.js';
import JunkieHeader from '../../Header/JunkieHeader.js';
import ParticipateButton from '../../ButtonComp/ParticipateButton.js';
import ContestRow from './ContestRow.js';
import background from '../../../Images/Trees-tunnel-road_1920x1200.jpg';
import AppHightContext from '../../../Contexts/AppHightContext.js';
import '../css-es/IndividualContestPage.css';

const Contest = (props) => {
    const { setHight } = useContext(AppHightContext);
    const [retrieve, setRetrieve] = useState(false);
    const [err, setError] = useState(false);
    const [checkForPartIn, setCheckPartInState] = useState([]);
    const [contest, setContestInfo] = useState(null);
    const [submission, setSubmission] = useState(false);
    useEffect(() => {
        setRetrieve(true);
        authService.getContestById(props.match.params.contestId)
            .then((res) => {
                if (res.message) {
                    throw new Error(`${res.message}`);
                } else {
                    setContestInfo(res.data);
                }
            })
            .catch(err => setError(err))
            .finally(() => setRetrieve(false));
    }, [])

    useEffect(() => {
        authService.getParticipatingContests()
            .then((res) => {
                if (res.message) {
                    throw new Error(`${res.message}`);
                } else {
                    setCheckPartInState([...res.data]);
                }
            })
            .catch(err => setError(err));
    }, [])

    useEffect(() => {
        setRetrieve(true);
        authService.getSubmissionByContestId(props.match.params.contestId)
            .then((res) => {
                if (res.data) {
                    setSubmission(res.data);
                }
            })
            .catch(err => setError(`${err}`))
            .finally(() => setRetrieve(false));
    }, [])

    useEffect(() => {
        setHight({
            height: '100vh',
            width: '100%',
            backgroundImage: `url(${background})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            backgroundAttachment: 'fixed',
        });
    }, [contest]);

    if (err) {
        return <Error message={err} />
    }

    if (retrieve) {
        return <Loader />
    }

    if (contest) {
        return (
            <div id='bg'>
                <JunkieHeader props={props} />
                <div id='infoContainer'>
                    <RedirectingButton name={'Go Back'} route={'/contests/open'} props={props} />
                    <ContestRow contest={contest} type={"open"} props={props} />
                    {checkForPartIn.some((c) => +c.id === +props.match.params.contestId) ?
                        submission ? <p style={{ marginLeft: '28%', color: 'white', fontSize: '20px' }}>You have submitted. Expect your score, after the end date</p> :
                            <RedirectingButton name={'Upload photo'} route={`/contests/${props.match.params.contestId}/${0}/submission`}
                                marginLeft={'48%'} props={props} />
                        : <ParticipateButton contest={contest} marginLeft={'48%'} props={props} />}
                </div>
            </div>
        )
    }

    return null;
}

export default Contest;