/* eslint-disable jsx-a11y/alt-text */
import React, { useContext, useEffect, useState } from 'react';
import authService from '../../../service/authService';
import Error from '../../Error/ErrorComp';
import Loader from '../../Loader/Loader';
import JunkieHeader from '../../Header/JunkieHeader';
import '../css-es/ContestsPages.css';
import contestPhoto from '../../../Images/images.jpg';
import background from '../../../Images/Trees-tunnel-road_1920x1200.jpg';
import { Card } from 'react-bootstrap';
import RedirectingButton from '../../ButtonComp/RedirectingButton';
import AppHightContext from '../../../Contexts/AppHightContext.js';

const PartInContests = (props) => {
    const { setHight } = useContext(AppHightContext);
    const [retrieve, setRetrieve] = useState(false);
    const [err, setError] = useState(false);
    const [contests, setContests] = useState(false);
    useEffect(() => {
        setRetrieve(true);
        authService.getParticipatingContests()
            .then((res) => {
                if (res.message) {
                    throw new Error(`${res.data.message}`);
                } else {
                    setContests(res.data);
                }
            })
            .catch(err => setError(`${err}`))
            .finally(() => setRetrieve(false));
    }, [])

    useEffect(() => {
        if (contests.length <= 4) {
            setHight({
                height: '100vh',
                width: '100%',
                backgroundImage: `url(${background})`,
                backgroundRepeat: 'no-repeat',
                backgroundSize: 'cover',
                backgroundAttachment: 'fixed',
            });
        } else {
            setHight({
                height: 'auto',
                width: '100%',
                backgroundImage: `url(${background})`,
                backgroundRepeat: 'no-repeat',
                backgroundSize: 'cover',
                backgroundAttachment: 'fixed',
            });
        }
    }, [contests]);

    if (err) {
        return <Error message={err} />
    }

    if (retrieve) {
        return <Loader />
    }

    if (contests) {
        return (
            <div id='bg' style={contests.length <= 4 ? { height: '92.4%' } : { height: 'auto' }}>
                <JunkieHeader props={props} />
                <div id='openContestsContainer' style={contests.length <= 4 ? { height: '100%' } : { height: 'auto' }}>
                    {!contests ? <p>You are not participating in any contests at this moment.</p> :
                        contests.map((contest) => {
                            return new Date(contest.end_date) < new Date() ?
                                <div key={`contestCard${contest.id}`} id={`contestCard${contest.id}`} className='contestCard'>
                                    <Card style={{ width: '14rem', padding: '0px' }}>
                                        <Card.Img variant="top" src={contestPhoto} />
                                        <Card.Body>
                                            <Card.Title>{contest.title}</Card.Title>
                                            <Card.Text>Ended</Card.Text>
                                            <RedirectingButton name={'See more'} route={`/contests/participating/${+contest.id}/${1}`} props={props} />
                                        </Card.Body>
                                    </Card>
                                </div>
                                :
                                <div key={`contestCard${contest.id}`} id={`contestCard${contest.id}`} className='contestCard'>
                                    <Card style={{ width: '14rem', padding: '0px' }}>
                                        <Card.Img variant="top" src={contestPhoto} />
                                        <Card.Body style={{ hight: '40px' }}>
                                            <Card.Title>{contest.title}</Card.Title>
                                            <Card.Text>On going</Card.Text>
                                            <RedirectingButton name={'See more'} route={`/contests/participating/${+contest.id}/${0}`} props={props} />
                                        </Card.Body>
                                    </Card>
                                </div>
                        })}
                </div>
            </div>
        )
    }

    return null;
}
export default PartInContests;