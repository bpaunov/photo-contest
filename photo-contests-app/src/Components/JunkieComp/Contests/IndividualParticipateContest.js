/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect, useState } from 'react';
import authService from '../../../service/authService.js';
import Error from '../../Error/ErrorComp.js';
import Loader from '../../Loader/Loader.js';
import JunkieHeader from '../../Header/JunkieHeader.js';
import GoBackButton from '../../ButtonComp/GoBackButton.js';
import ContestRow from './ContestRow.js';
import background from '../../../Images/Trees-tunnel-road_1920x1200.jpg';
import AppHightContext from '../../../Contexts/AppHightContext.js';
import '../css-es/IndividualContestPage.css';
const PartOfContest = (props) => {
    const { setHight } = useContext(AppHightContext);
    const [retrieve, setRetrieve] = useState(false);
    const [err, setError] = useState(false);
    const [contest, setContestInfo] = useState();

    useEffect(() => {
        setRetrieve(true);
        authService.getContestById(props.match.params.contestId)
            .then((res) => {
                if (res.message) {
                    throw new Error(`${res.message}`);
                } else {
                    console.log(res.data);
                    setContestInfo(res.data);
                }
            })
            .catch(err => setError(err))
            .finally(() => setRetrieve(false));
    }, [])

    useEffect(() => {
        setHight({
            height: '100vh',
            width: '100%',
            backgroundImage: `url(${background})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            backgroundAttachment: 'fixed',
        });
    }, [contest]);

    if (err) {
        return <Error message={err} />
    }

    if (retrieve) {
        return <Loader />
    }

    if (contest) {
        return (
            <div id='bg'>
                <JunkieHeader props={props} />
                <div id='infoContainer'>
                    <GoBackButton name={'Go Back'} props={props} />
                    {+props.match.params.isFinished === 1 ?
                        <ContestRow contest={contest} type={'finished'} comesFrom={'individualPart'} props={props} /> :
                        <ContestRow contest={contest} type={'onGoing'} comesFrom={'individualPart'} props={props} />}
                </div>
            </div>
        )
    }

    return null;
}

export default PartOfContest;