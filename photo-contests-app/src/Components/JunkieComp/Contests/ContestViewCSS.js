import React, { Fragment } from 'react';
import moment from 'moment';
import { ListGroup, ListGroupItem } from 'react-bootstrap';

const CommonInfoContestViewCss = ({ contestData, props }) => {
    return (
        <Fragment>
            <ListGroup as="ul">
                <ListGroupItem as="li" variant="info" active>Category: {contestData.category}</ListGroupItem>
                <ListGroupItem>Title: {contestData.title}</ListGroupItem>
                <ListGroupItem>Start date: {moment(new Date(contestData.start_date)).format('l')}</ListGroupItem>
                <ListGroupItem>End date: {moment(new Date(contestData.end_date)).format('l')}</ListGroupItem>
            </ListGroup>
        </Fragment>
    )
}

export default CommonInfoContestViewCss;