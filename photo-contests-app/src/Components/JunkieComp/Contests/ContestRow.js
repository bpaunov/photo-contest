/* eslint-disable react-hooks/exhaustive-deps */
import React, { Fragment, useEffect, useState } from 'react';
import authService from '../../../service/authService.js';
import Timer from '../RemainingTimeComp/Timer';
import Error from '../../Error/ErrorComp';
import Loader from '../../Loader/Loader';
import RedirectingButton from '../../ButtonComp/RedirectingButton.js';
import CommonInfoContestViewCss from './ContestViewCSS.js';
import { ListGroup, ListGroupItem } from 'react-bootstrap';

const ContestRow = ({ contest, type, comesFrom, props }) => {
    const [retrieve, setRetrieve] = useState(false);
    const [err, setError] = useState(false);
    const [submission, setSubmission] = useState(false);
    const [isInPhase1, setIsInPhase1State] = useState(false);
    useEffect(() => {
        setRetrieve(true);
        authService.getSubmissionByContestId(contest.id)
            .then((res) => {
                if (res.data) {
                    setSubmission(res.data);
                }
            })
            .catch(err => setError(`${err}`))
            .finally(() => setRetrieve(false));
    }, [])

    useEffect(() => {
        authService.getSingleContestP1(contest.id)
            .then((res) => {
                if (new Date(res.data.end_date) > new Date()) {
                    setIsInPhase1State(true);
                }
            })
            .catch(err => setError(`${err}`));
    }, [])

    if (err) {
        return <Error message={err} />
    }

    if (retrieve) {
        return <Loader />
    }

    if (type === 'finished') {
        return (
            <div id={`contest${contest.id}`} style={{ width: '50%', margin: 'auto' }}>
                <ListGroup style={{ marginBottom: '30px' }}>
                    <ListGroupItem><CommonInfoContestViewCss contestData={contest} props={props} /></ListGroupItem>
                </ListGroup>
                {submission ?
                    <RedirectingButton name={'View your score'} marginLeft={'38%'}
                        route={`/contests/${contest.id}/submission/${submission.id}/score`} props={props} /> :
                    <p style={{ marginLeft: '33%', fontSize: '25px', color: 'white' }}>You missed to submit</p>}
                {comesFrom !== 'individualPart' &&
                    <RedirectingButton name={'See more'} marginLeft={'48%'} route={`/contests/participating/${+contest.id}/${1}`} props={props} />}
            </div>
        )
    }

    if (type === 'onGoing') {
        return (
            <div id={`contest${contest.id}`} style={{ width: '50%', margin: 'auto' }}>
                <ListGroup style={{ marginBottom: '30px' }}>
                    <ListGroupItem><CommonInfoContestViewCss contestData={contest} props={props} /></ListGroupItem>
                    {isInPhase1 && <ListGroupItem style={{ backgroundColor: 'green', color: 'white' }}>
                        <Timer endDate={contest.end_date} props={props} />
                    </ListGroupItem>}
                </ListGroup>
                {comesFrom === 'individualPart' ?
                    submission ?
                        <p style={{ marginLeft: '20%', color: 'white' }}>You have submitted. Expect your score, after the end date</p> :
                        isInPhase1 ?
                            <RedirectingButton name={'Upload photo'} marginLeft={'38%'} route={`/contests/${contest.id}/${1}/submission`}
                                props={props} />
                            :
                            <p style={{ marginLeft: '33%', fontSize: '25px', color: 'white' }}>You missed to submit</p>
                    :
                    submission ?
                        <Fragment>
                            <p style={{ marginLeft: '33%', color: 'white' }}>You have submitted. Expect your score, after the end date</p>
                            <RedirectingButton name={'See more'} marginLeft={'38%'} route={`/contests/participating/${contest.id}/${0}`} props={props} />
                        </Fragment> :
                        <Fragment>
                            <p style={{ marginLeft: '33%', color: 'white' }}>Participating in</p>
                            <RedirectingButton name={'See more'} marginLeft={'38%'} route={`/contests/participating/${contest.id}/${0}`} props={props} />
                        </Fragment>}
            </div>
        )
    }

    if (new Date(contest.end_date) > new Date() && type === 'open') {
        return (
            <div id={`contest${contest.id}`} style={{ width: '50%', margin: 'auto', marginBottom: '30px' }}>
                <ListGroup>
                    <ListGroupItem><CommonInfoContestViewCss contestData={contest} props={props} /></ListGroupItem>
                    <ListGroupItem style={{ backgroundColor: 'green', color: 'white' }}><Timer endDate={contest.end_date} props={props} /></ListGroupItem>
                </ListGroup>
            </div >
        )
    }

    return (
        null
    )
}

export default ContestRow;