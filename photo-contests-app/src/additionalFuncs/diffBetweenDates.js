export const diffBetweenDates = (date1, date2) => {
    const getDifferenceInDays = (date1, date2) => {
        const diffInMs = Math.abs(date2 - date1);
        return Math.floor(diffInMs / (1000 * 60 * 60 * 24));
    }

    const getDifferenceInHours = (date1, date2) => {
        const diffInMs = Math.abs(date2 - date1);
        return Math.floor(diffInMs / (1000 * 60 * 60)) - Math.floor(diffInMs / (1000 * 60 * 60 * 24)) * 24;
    }

    const getDifferenceInMinutes = (date1, date2) => {
        const diffInMs = Math.abs(date2 - date1);
        return Math.floor(diffInMs / (1000 * 60)) - Math.floor(diffInMs / (1000 * 60 * 60)) * 60;
    }

    const getDifferenceInSeconds = (date1, date2) => {
        const diffInMs = Math.abs(date2 - date1);
        return Math.floor(diffInMs / 1000) - Math.floor(diffInMs / (1000 * 60)) * 60;
    }

    let seconds = getDifferenceInSeconds(new Date(date1), date2);
    let minutes = getDifferenceInMinutes(new Date(date1), date2);
    let hours = getDifferenceInHours(new Date(date1), date2);
    let days = getDifferenceInDays(new Date(date1), date2);

    return {
        seconds: seconds,
        minutes: minutes,
        hours: hours,
        days: days
    }
}