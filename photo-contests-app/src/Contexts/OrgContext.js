import React from 'react';

const OrgContext = React.createContext({
    CONTEST_ID: null,
    SET_CONTEST_ID: () => { }
});

export default OrgContext;