# Photo Contest

Competition management system
Single-page application that allows for an easy management of online photo contests.
The application's structure is divided into a public part - accessible without authentication and a private part - visible only after the user has successfully registered and signed in.
The users of the application are divided into:
  - Photo Junkies - who can create submissions and enroll into contests.
  - Organizers - who can perform CRUD operations onto the contest form itself and the jury of the contest as well as the ability to manage the submissions of the photo junkies.
The system implements a scoring functionality which awards the users with points and placement according to an average jury score for their submission.
The users are segregated into different levels with different privileges according to the points they have amassed.
